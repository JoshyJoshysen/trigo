//packages
var express       = require('express'),
	multer 		  = require('multer'),
    mongoose      = require('mongoose'),
    session       = require('express-session'),
    mongoStore    = require('connect-mongo')(session),
    flash         = require('connect-flash'),
    path          = require('path'),
    favicon       = require('serve-favicon'),
    logger        = require('morgan'),
    cookieParser  = require('cookie-parser'),
    bodyParser    = require('body-parser'),
    passport      = require('passport');

//routes
var routes        = require('./routes/index'),
    authoring     = require('./routes/authoring'),
    visualizing   = require('./routes/visualizing'),
    auth          = require('./routes/auth'),
    api           = require('./routes/api'),
    downloads     = require('./routes/downloads'),
    basics        = require('./routes/basics'),
    log           = require('./routes/logger'),
    test          = require('./routes/test');

//models
var models        = require('./models'),
    Activity,         //Activity mongoose model
    //CollectedData,  //Collected Data mongoose model
    User;             //User mongoose model

var app = express();


//test sockeio
//Socket.io
var socket_io    = require( "socket.io" );
var io = socket_io();
app.io = io;
var socketIO ; 
io.on( "connection", function(socket){
   socketIO = socket;
   app.socket = socketIO;
});

//config for the mongobd connection
var dbconf = {
  db: {
    db: 'trigo',
    host: 'localhost',
    port:  27017  //20763
    //username: 'trigodb',
    //password: 'tripwd',
    //collection: 'session'
  },
  secret: 'ubersecret'
};

//setting up the mulder for uploading file purposes
app.use(multer({ dest: './uploads/'}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/img/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
//session support
app.use(session({
  cookie: {
    //maxAge: new Date(Date.now() + 3600000)
  },
  secret: dbconf.secret ,
  resave: true,
  saveUninitialized: true,
  store:new mongoStore(dbconf.db)
}));
//authentication
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Models for the database and exports
 */
models.defineModels(mongoose, function () {
  var dbUrl = 'mongodb://';
  //dbUrl += dbconf.db.username+':'+dbconf.db.password+'@';
  dbUrl += dbconf.db.host + ':' + dbconf.db.port;
  dbUrl += '/' + dbconf.db.db;
  app.User = User = mongoose.model('User');
  app.Activity = Activity = mongoose.model('Activity');
  db = mongoose.connect(dbUrl);
});


global.appRoot = path.resolve(__dirname);

exports.getUser = function(){
	  var u = User;
	  return u;
	}
exports.getIo = function(){
	var ioR = io;  
		  return ioR;
}

exports.getSocket = function(){
	var socketio = socketIO;  
		  return socketio;
}
exports.newUser = function(params){
  var u = new User(params);
  return u;
}

exports.getActivity = function(){
  var a = Activity;
  return a;
}

auth.initializeLogin();

/**
 * routes
 */

//index
app.use('/', routes);

//authoring
app.use('/authoring', authoring);

//authoring
app.use('/visualizing', visualizing);

//downloads
app.use('/downloads', downloads);

//logger
app.use('/logger', log);

//test
app.use('/test', test);

//API
app.use('/api', api);

//basic informations
app.use('/basics', basics);

//authentication
app.get('/login', auth.index);
app.get('/logout', auth.loggedIn, auth.logout);

app.post('/auth/local', auth.local);
// Redirect the user to Facebook for authentication.  When complete, Facebook
// will redirect the user back to the application at
//   /auth/facebook/callback
app.get('/auth/facebook', auth.facebook);
// Facebook will redirect the user to this URL after approval.
app.get('/auth/facebook/callback', auth.facebookCallback);
// Redirect the user to Twitter for authentication.  When complete, Twitter
// will redirect the user back to the application at
//   /auth/twitter/callback
app.get('/auth/twitter', auth.twitter);
// Twitter will redirect the user to this URL after approval.
app.get('/auth/twitter/callback', auth.twitterCallback);
// Redirect the user to Google for authentication.  When complete, Google
// will redirect the user back to the application at
//     /auth/google/callback
app.get('/auth/google', auth.google);
// Google will redirect the user to this URL after authentication.
app.get('/auth/google/callback', auth.googleCallback);

//registration
app.get('/register', auth.reg);
app.post('/register/newuser', auth.register);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
