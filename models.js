//var Scenario;
//var CollectedData;
var User;
var Activity;
var fs = require('fs');

function extractKeywords(text) {
  if (!text) return [];

  return text.
    split(/\s+/).
    filter(function(v) { return v.length > 2; }).
    filter(function(v, i, a) { return a.lastIndexOf(v) === i; });
}

function defineModels(mongoose, fn) {
  var Schema = mongoose.Schema;

  /**
   * Model: User
   */
  User = new Schema({
    'username': String,
    'password': String,
    'oauthID': String
  });

  mongoose.model('User', User);

  /**
   * Model: Scenario
   */
  Activity = new Schema({
    'title': String,
    'description': String,
    'location': String,
    'questionnaire': Boolean,
    'data': {},
    'user': { type: Schema.Types.ObjectId, ref: 'User' },
    'createdAt': { type : Date, default: Date.now  },
    'lastModified': { type : Date, default: Date.now  },
    'code': String,
    'password': String,
    'teacherpwd': String,
    'submitted': [],
    'downloaded_xmls': [
      {
        xml_index: {type: String},
        date: {type: Date}
      }
    ],
    'uploaded_xmls': [
      {
        xml_index: {type: String},
        date: {type: Date},
        filename: {type: String}
      }
    ]
  });

  mongoose.model('Activity', Activity);

  fn();
}

exports.defineModels = defineModels;

