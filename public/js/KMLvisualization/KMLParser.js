

var KMLParser = new function () {

    /*this.StylesList = new Array();
    this.StyleItems = new Array();
    this.TasksList = new Array();
    this.TasksItems = new Array();
    this.TaskItem = new Array();
    var group_name = '';*/

    this.activity_name = '';

    var dom;

    this.getInstance = function(){
        if (this.dom==null){
            this.dom = new Object();
            //console.log("creating instance xml parser!");
        }
        return this.dom;
    };

    this.init = function(doc) {

        var parseXml;

        if (window.DOMParser) {
            parseXml = function(xmlStr) {
                return ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");
            };
        } else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {
            parseXml = function(xmlStr) {
                var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = "false";
                xmlDoc.loadXML(xmlStr);
                return xmlDoc;
            };
        } else {
            parseXml = function() { return null; }
        }
        this.doc = parseXml(doc);
        //console.log(this.doc);

        // ID of the activity
        this.id = this.doc.getElementsByTagName("name")[0].childNodes[1].textContent;
        this.id = this.id.replace('\n','');
        this.id = this.id.replace('\n','');
        PointList.setGroupName(this.id);
        //console.log(this.id);

        this.activity_name = this.id;


        //Number of Style items
        //console.log("number of Styles: " + this.doc.getElementsByTagName("Style").length);

        //all the Styles (6 points + 3 states + 10 tasks)
        for (var i=0; i<this.doc.getElementsByTagName("Style").length; i++) {

            //get Style
            this.style = this.doc.getElementsByTagName("Style")[i];

            //get ID of the Style
            this.style_id = this.style.getAttribute("id");

            //get icon of the style
            this.iconStyle = this.style.getElementsByTagName("IconStyle")[0];
            this.icon = this.iconStyle.getElementsByTagName("Icon")[0];
            this.iconHref = this.icon.getElementsByTagName("href")[0].childNodes[0].nodeValue;

            var SI = new StyleItem(this.style_id, this.iconHref);
            PointList.getStyleList().push(SI);


            //console.log("Group name: " + this.group_name + ", Style ID: " + this.style_id + ", Icon href: " + this.iconHref);
        }
        //console.log(PointList);

        //console.log(this.doc.getElementsByTagName('Folder').length);

        for (var group=0; group < this.doc.getElementsByTagName('Folder').length; group ++) {

            this.tasks = this.doc.getElementsByTagName("Folder")[group];

            this.group_name = this.tasks.getElementsByTagName("name")[0].childNodes[1].textContent;
            this.group_name = this.group_name.replace('\n','');

            //for each task
            for (var i=0; i<this.tasks.getElementsByTagName("Folder").length; i++) {

                //get Folder (task)
                this.task = this.tasks.getElementsByTagName("Folder")[i];

                //get task name
                this.task_name = this.task.getElementsByTagName("name")[0].childNodes[1].textContent;
                this.task_name = this.task_name.replace('\n','');
                this.task_name = this.task_name.replace('\n','');

                //console.log("number of attemps in this point: " + this.task.getElementsByTagName("Placemark").length);

                //for each attempt
                for (var j = 0; j < this.task.getElementsByTagName("Placemark").length; j++) {

                    //get Placemark (Attempt)
                    this.placemark = this.task.getElementsByTagName("Placemark")[j];

                    this.placemark_name = this.placemark.getElementsByTagName("name")[0].childNodes[1].textContent;
                    this.placemark_name = this.placemark_name.replace('\n','');
                    this.placemark_name = this.placemark_name.replace('\n','');
                    this.placemark_ref = this.placemark.getElementsByTagName("styleUrl")[0].childNodes[0].nodeValue;
                    this.point = this.placemark.getElementsByTagName("coordinates")[0].childNodes[0].nodeValue;
                    this.point = this.point.replace('\n','');

                    var TI = new TaskItem(this.group_name, this.task_name, this.placemark_name, this.point, this.placemark_ref);
                    PointList.getTaskList().push(TI);

                    //console.log(k + ": Placemark name: " + this.placemark_name + ", Placemark point: " + this.point);

                    //this.TaskItem.push(this.placemark_name, this.point);

                    //this.TasksItems.push(this.task_name, this.TaskItem);

                    //this.TasksList.push(this.TasksItems);
                }
            }
        }

    };

}
