var PointList = new function() {

    this.styleList;
    this.taskList;

    this.groupname;

    this.getInstance = function() {
        if (!this.list) {
            this.styleList = new Array(); //Array StyleItem
            this.taskList = new Array(); //Array TaskItem
        }
    };

    this.getStyleList = function() {
        return this.styleList;
    };

    this.getTaskList = function() {
        return this.taskList;
    };

    this.getGroupName = function() {
        return this.groupname;
    };

    this.setGroupName = function(groupname) {
        this.groupname = groupname;
    };

}