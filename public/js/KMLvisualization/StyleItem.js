
function StyleItem(id, icon) {

    //console.log('Style item created: id: ' + id + ', icon: ' + icon);
    this.id = id;
    this.icon = icon;
}


StyleItem.prototype.getId = function() {
    return this.id;
}

StyleItem.prototype.getIcon = function() {
    return this.icon;
}