
function TaskItem(group, task, name, coordinates, ref) {

    //console.log('Task item created: name: ' + name + ', coordinates: ' + coordinates);
    this.group = group;
    this.task = task;
    this.name = name;
    this.coordinates = coordinates;
    this.ref = ref;
}

TaskItem.prototype.getGroup = function() {
    return this.group;
}

TaskItem.prototype.getTask = function() {
    return this.task;
}

TaskItem.prototype.getName = function() {
    return this.name.split(/[\s,]+/);

}

TaskItem.prototype.getCoordinates = function() {
    return this.coordinates.split(',');
}

TaskItem.prototype.getRef = function() {
    return this.ref.replace('#','');
}
