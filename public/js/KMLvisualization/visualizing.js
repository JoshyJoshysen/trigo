
var myApp = angular.module('myApp', ['ngRoute', 'checklist-model', 'ui.bootstrap']);

myApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/maps', {
                templateUrl: '/js/KMLvisualization/templates/maps.html'
            }).
            otherwise({
                redirectTo: '/maps'
            });
    }]);

myApp.controller('mapsCtrl', function($scope, $http) {

    //Triangle icon for the dropdown menus
    $scope.toggleIcon = function(id) {
        if (document.getElementById('icon' + id).className == 'glyphicon glyphicon-triangle-right') {
            document.getElementById('icon' + id).className = 'glyphicon glyphicon-triangle-bottom';
            logger('Open dropdown: ' + id);
        } else {
            document.getElementById('icon' + id).className = 'glyphicon glyphicon-triangle-right';
            logger('Close dropdown: ' + id);
        }
    };

    var groupsArray = [];
    var selectedItems = [];

    //Title of the activity
    $scope.activityTitle = activityTitle;


    //$http.get('http://localhost:3232/files' + '/' + 'Example1.kml').
    //$http.get('http://celtest1.lnu.se:3232/files/' + activityID + '/' + activityID  +'.kml')
    $http.get("/api/createKML/"+activityID+"/"+selected).
        success(function(data) {
            KMLParser.getInstance();
            PointList.getInstance();
            KMLParser.init(data);
            initialize(data);

            $scope.activity_name = 'Activity: ' + KMLParser.activity_name;
        });



    function initialize(file) {

        var attemptsLIST = [];
        var tasksLIST = [];
        var groupsLIST = [];

        var targetPoints = [];

        //data array is the array for the markers that are displayed
        var data = [];
        $scope.user = { data: [] };
        var markers = [];

        var file = file;

        $scope.selected_markers = [];


        //hide map - show white background -> dynamic size squares depending on the zoom
        function CoordMapType(tileSize) {
            this.tileSize = tileSize;
        }
        CoordMapType.prototype.getTile = function(coord, zoom, ownerDocument) {
            var div = ownerDocument.createElement('div');
            //div.innerHTML = coord;
            div.style.width = this.tileSize.width + 'px';
            div.style.height = this.tileSize.height + 'px';
            //div.style.fontSize = '10';
            //div.style.borderStyle = 'solid';
            //div.style.borderWidth = '1px';
            //div.style.borderColor = '#AAAAAA';
            div.style.backgroundColor = '#FFFFFF';
            return div;
        };

        //create the map and center it in the first task point
        var mapProp = {
            center:new google.maps.LatLng(PointList.getTaskList()[0].getCoordinates()[1],PointList.getTaskList()[0].getCoordinates()[0]),
            zoom: 19,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        };


        var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

        var hideBackground = function() {
            map.overlayMapTypes.insertAt(0, new CoordMapType(new google.maps.Size(256, 256)));
        };

        var showBackground = function() {
            map.overlayMapTypes.clear();
        };

        var parseXml;


        if (window.DOMParser) {
            parseXml = function(xmlStr) {
                return ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");
            };
        } else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {
            parseXml = function(xmlStr) {
                var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = "false";
                xmlDoc.loadXML(xmlStr);
                return xmlDoc;
            };
        } else {
            parseXml = function() { return null; }
        }
        this.doc = parseXml(file);
        //console.log(this.doc);

        var groupCont = 0;



        for (var group=0; group < this.doc.getElementsByTagName('Folder').length; group ++) {
            this.tasks = this.doc.getElementsByTagName("Folder")[group];

            this.group_name = this.tasks.getElementsByTagName("name")[0].childNodes[1].textContent;
            this.group_name = this.group_name.replace('\n','');

            if (this.group_name.substring(0,5) == 'Grupp') { //only groups
                //console.log(this.group_name);
                //for each task
                tasksLIST = [];
                for (var i=0; i<this.tasks.getElementsByTagName("Folder").length; i++) {
                    //get Folder (task)
                    this.task = this.tasks.getElementsByTagName("Folder")[i];
                    //get task name
                    this.task_name = this.task.getElementsByTagName("name")[0].childNodes[1].textContent;
                    this.task_name = this.task_name.replace('\n','');
                    this.task_name = this.task_name.replace('\n','');
                    //console.log(this.task_name);

                    attemptsLIST = [];
                    //lastAttempt is used to display the information in a new position, as in the app.
                    var lastAttempt = [];
                    //for each attempt
                    for (var j = 0; j < this.task.getElementsByTagName("Placemark").length; j++) {

                        //get Placemark (Attempt)
                        this.placemark = this.task.getElementsByTagName("Placemark")[j];

                        this.placemark_name = this.placemark.getElementsByTagName("name")[0].childNodes[1].textContent;
                        this.placemark_name = this.placemark_name.replace('\n', '');
                        this.placemark_name = this.placemark_name.replace('\n', '');
                        this.placemark_name = this.placemark_name.split(/,| /);
                        this.placemark_ref = this.placemark.getElementsByTagName("styleUrl")[0].childNodes[0].nodeValue;
                        this.point = this.placemark.getElementsByTagName("coordinates")[0].childNodes[0].nodeValue;
                        this.point = this.point.replace('\n','');

                        this.icon = pathImage(this.placemark_ref);

                        var attempt = [];

                        var target = [];

                        if (this.placemark_name[0] == 'Punkt-1') {
                            lastAttempt = [this.placemark_name[0], this.placemark_name[1], this.icon, this.point, 'title1', i + '_' + groupCont];

                            target = [0, 0, this.icon, this.point, 'marker', 0];
                            targetPoints.push(target);

                        } else {
                            if (this.placemark_name[0] == 'Punkt-2') {
                                attempt = [lastAttempt[0], lastAttempt[2], this.icon, this.point, 'title1', i + '_' + groupCont];
                                attemptsLIST.push(attempt);
                                attempt = [this.placemark_name[0], lastAttempt[1], this.placemark_name[1], this.point, 'title2', i + '_' + groupCont];
                                attemptsLIST.push(attempt);


                                target = [0, 0, this.icon, this.point, 'marker', 0];
                                targetPoints.push(target);


                            } else {
                                attempt = [this.placemark_name[0], this.placemark_name[1], this.icon, this.point, 'notitle', i + '_' + groupCont];
                                attemptsLIST.push(attempt);
                            }
                        }

                    }
                    var task = [this.task_name, attemptsLIST];
                    tasksLIST.push(task);
                    var gr = [this.group_name, tasksLIST];




                }
                groupsLIST.push(gr);
                //console.log(groupsLIST);

                groupCont ++;

            } //if is a group



        }

        // show the map or a white background
        $scope.map = function() {
            if (map.overlayMapTypes.length == 0) {
                hideBackground();
            } else {
                showBackground();
            }
        }

        //console.log(targetPoints);

        function eliminateDuplicates(arr) {

            var out = [];
            if (out.length == 0) {
                out.push(arr[0]);
            }
            for (var i=0; i<arr.length; i++) {
                var dif = 0;
                for (var j=0; j<out.length; j++) {
                    //console.log(i + ' _ ' + j + ': ' + arr[i][2] + ' - ' + out[j][2]);
                    if (arr[i][2] == out[j][2]) {
                        //console.log(arr[i][2] + ' - ' + out[j][2]);
                        dif = 1;
                    }
                }
                if (dif == 0) {
                    out.push(arr[i]);
                    //console.log(JSON.stringify(out));
                }
            }
            return out;
        }

        targetPoints = eliminateDuplicates(targetPoints);
        $scope.targetPoints = targetPoints;

        for (var i=0; i<targetPoints.length; i++) {
            $scope.user.data.push(targetPoints[i]);
        }

        data = groupsLIST;

        //Uncheck all the tasks for a selected group
        $scope.uncheckAll = function(item) {
            for (var i=0; i<groupsLIST.length; i++) { //each group
                for (var j=0; j<groupsLIST[i][1].length; j++) { //each task
                    for (var k=0; k<groupsLIST[i][1][j][1].length; k++) { //each attempt
                        if (groupsLIST[i][1][j][1][k][5] == item) {
                            for (var w=0; w<$scope.user.data.length; w++) { //iterate array of data that is marked
                                if ($scope.user.data[w][5] == item) {
                                    if (($scope.user.data[w][0] != 'Punkt-1') && ($scope.user.data[w][0] != 'Punkt-2')) { //Don't delete the Markers
                                        var index = $scope.user.data.indexOf($scope.user.data[w]);
                                        $scope.user.data.splice(index, 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            document.getElementById("unselectBtn_task" + item).style.display = "none";
            document.getElementById("selectBtn_task" + item).style.display = "initial";
            updateMarkers();
            logger('Unselect All in task: ' + item);
        };



        //Check all the tasks for a selected group
        $scope.checkAll = function(item) {
            for (var i=0; i<groupsLIST.length; i++) { //each group
                for (var j=0; j<groupsLIST[i][1].length; j++) { //each task
                    for (var k=0; k<groupsLIST[i][1][j][1].length; k++) { //each attempt
                        if (groupsLIST[i][1][j][1][k][5] == item) {
                            if ((groupsLIST[i][1][j][1][k][0] != 'Punkt-1') && (groupsLIST[i][1][j][1][k][0] != 'Punkt-2')) {  //Don't add the Markers
                                $scope.user.data.push(groupsLIST[i][1][j][1][k]);
                            }
                        }
                    }
                }
            }
            //console.log(groupsLIST);
            document.getElementById("selectBtn_task" + item).style.display = "none";
            document.getElementById("unselectBtn_task" + item).style.display = "initial";
            updateMarkers();
            logger('Select All in task: ' + item);
        };

        //Call updateMarkers() from the view
        $scope.updateMarkers = function() {
            updateMarkers();
            //console.log($scope.user.data);
        };

        // Update markers that will appear in the map
        var updateMarkers = function() {

            // Init markers array to null
            for (var i=0; i<markers.length; i++) {
                markers[i].setMap(null);
            }

            //console.log($scope.targetPoints);
            //console.log($scope.user.data);

            // Refill markers array with the ones in the $scope.user.data
            for (var x=0; x< $scope.user.data.length; x++) {

                var coords = $scope.user.data[x][3].split(', ');
                //console.log(coords);

                //var id = $scope.user.data[x][0];
                var latlon = new google.maps.LatLng(coords[1], coords[0]);
                var image = $scope.user.data[x][2];

                //console.log(latlon);
                image = {url: image, scaledSize: new google.maps.Size(28, 28)};

                if (image != undefined) {
                    var marker = new google.maps.Marker({
                        position: latlon,
                        map: map,
                        title: "Here",
                        icon: image
                    });
                    markers.push(marker);
                }
            }

            var tasksArray = [];
            var attemptsArray = [];
            var aux;
            var aux1;

            selectedItems = [];
            for (var a = 0; a < $scope.user.data.length; a ++) {
                if (selectedItems.indexOf($scope.user.data[a][0]) == '-1') { //if the group does not exist...
                    aux = $scope.user.data[a][0];
                    tasksArray = [];
                    for (var aa = 0; aa < $scope.user.data.length; aa ++) {
                        if ($scope.user.data[aa][0] == aux) { //if it is the same group
                            if (tasksArray.indexOf($scope.user.data[aa][1]) == '-1') { //if the task does not exist
                                aux1 = $scope.user.data[aa][1];
                                attemptsArray = [];
                                for (var aaa = 0; aaa <$scope.user.data.length; aaa ++) {
                                    if (($scope.user.data[aaa][0] == aux) && ($scope.user.data[aaa][1] == aux1)) {
                                        attemptsArray.push([$scope.user.data[aaa][2], $scope.user.data[aaa][3], $scope.user.data[aaa][5]]);
                                    }
                                }
                                tasksArray.push($scope.user.data[aa][1], attemptsArray);
                            }
                        }
                    }
                    selectedItems.push($scope.user.data[a][0], tasksArray);
                    selectedItems.push(targetPoints);
                }
            }
            //console.log(selectedItems);
            //logger('Click: ' + selectedItems);
        };

        // Display the tasks

        $scope.data = data;

        $scope.group_name = groupsArray[0]; // group name


        var tasks = [];
        var task_name = [];
        var task_array = [];


        tasks.push([task_name, task_array]);
        //console.log(tasks);
        $scope.tasks = tasks;
        $scope.points = task_array;

        updateMarkers();


    }


    // Function to find the correct URl of the image if it is an attempt
    var pathImage = function(image) {
        var path = 'http://trigo.lnu.se:80/img/icons/';
        //console.log('correcting: ' + image);

        switch (image) {
            case '#1':
                image = 'cat.png';
                break;
            case '#2':
                image = 'house.png';
                break;
            case '#3':
                image = 'tree.png';
                break;
            case '#4':
                image = 'bike.png';
                break;
            case '#5':
                image = 'car.png';
                break;
            case '#6':
                image = 'horse.png';
                break;
            case '#ok':
                image = 'ok.png';
                break;
            case '#rejected':
                image = 'rejected.png';
                break;
            default:
                if (image != '') {
                    var img = image.split('t'); // letter t from the word poinT
                    img = img[1];
                    if (parseInt(img) < 10) {
                        img = '0' + img;
                    }
                    //console.log(img);
                    image = 'http://google-maps-icons.googlecode.com/files/red' + img + '.png';
                    return image;
                }
                break;
        }

        return path + image;

    };

    window.onclick = function(e) {
        //logger('Click: ' + e.srcElement.className);
    };

    //Call the logger() function from the view
    $scope.logger = function(inf) {
        logger(inf);
    };

    var logger = function(info) {
        var log = Array();
        var displayDate = new Date();

        log.push([activityID, displayDate, info]);
        console.log ('Log: ' + JSON.stringify(log));

        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
        xmlhttp.open("POST", "/logger/logevent" ,"/json-handler");
        //xmlhttp.open("POST", "http://www.hepeventos.com/Trigo/echo.php" ,"/json-handler");
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(log);
    }

});


