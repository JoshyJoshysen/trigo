/**
 * Created by aaml on 12.02.2015.
 */
$(function () {
  var mapSelector = $('#map');
  var mapWidget;
  var tennisCourt = new TennisCourt();
  var t;


  if (mapSelector)
    mapWidget = initMap();

  mapSelector.droppable({
    accept: ".tennis-court",
    drop: function (event, ui) {
          //ui.draggable.hide();
          document.getElementById('trashElement').style.visibility = 'visible';
          if (t) {
              var r = confirm("Are you sure you want to change the position of this element?");
              if (r == true) {
                  t.getPolygon().setMap(null);
                  t.setMarkers([]);
                  t.removeRotateIcon();

                  t = tennisCourt;
                  t.init(mapWidget.getMap().getCenter());
                  t.setMap(mapWidget.getMap());
                  //tennisCourt.setCenter();
                  markers = createMarkers();
                  t.setMarkers(markers);
              } else {
                  // no changes if we do not replace the element
              }

          } else {
              t = tennisCourt;
              t.init(mapWidget.getMap().getCenter());
              t.setMap(mapWidget.getMap());
              //tennisCourt.setCenter();
              markers = createMarkers();
              t.setMarkers(markers);
              document.getElementById('addElement').style.visibility = 'hidden';
          }

      }

  });

  google.maps.event.addListener(mapWidget.getMap(), 'zoom_changed', function() {
    if (t){
      if (mapWidget.getMap().zoom >= 17){
        t.getRotateHandler().setVisible(true);
      } else {
        t.getRotateHandler().setVisible(false);
      }
    }
  });

  $('span#trashElement').click(function () {
      if (t) {
          var r = confirm("Are you sure you want to delete this element from the map?");
          if (r == true) {
              t.getPolygon().setMap(null);
              t.setMarkers([]);
              t.removeRotateIcon();
              t = null;
              document.getElementById('trashElement').style.visibility = 'hidden';
              document.getElementById('addElement').style.visibility = 'visible';
          } else {
                // no changes if we do not delete the element
          }
      }
  });

  $('span#addElement').click(function () {
      t = tennisCourt;
      t.init(mapWidget.getMap().getCenter());
      t.setMap(mapWidget.getMap());
      //tennisCourt.setCenter();
      markers = createMarkers();
      t.setMarkers(markers);
      document.getElementById('addElement').style.visibility = 'hidden';
      document.getElementById('trashElement').style.visibility = 'visible';
    });

  $('div#save-scenario').click(function () {
	  if (t == null) { //image not dropped
          alert("Unable to find the points, please drop the image on the map");
      } else {
		  if (($('#scenario-title').val() != "") && ($('#scenario-description').val()!="")){
				var points = {};
			    var tennisCourtPoints = t.getPolygon().getPath();
			    tennisCourtPoints.forEach(function (point, index) {
			      tmpPoint = 'point'+index;
			      points[tmpPoint] = {
			        'lat' : point.lat(),
			        'lng' : point.lng()
			      };
			    });
			    create(points);
			 }
			 else
				 alert("The activity must have a name and a description");
	  }
  });
});

function initMap() {
  return new MapWidget();
}

/**
 * @ToDO has two tasks prepare and send, refactoring needed
 * prepare Activity Object and give this Object to the send Method
 * @param points
 */
function create(points) {
  console.log(points.point0);
  var latlng = points.point0.lat+","+points.point0.lng;
  $.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+latlng, function( data ) {
    console.log("location: "+data.results[0].formatted_address);
    var activity = {
      title       : $('#scenario-title').val(),
      description : $('#scenario-description').val(),
      data        : points,
      location    : data.results[0].formatted_address
    };

    sendActivityData(activity);
  });


};

/**
 * Send Activity Object to the save Instance
 * @param activityData
 */
function sendActivityData(activityData) {
  $.post("/authoring/save", activityData, function (data) {
      window.location = "/authoring/activity/"+data._id;
    },
    "json"
  );
}

/**
 * Build Google Maps marker with the images
 * Marker is not yet set to a map, task for the MapWidget
 * @returns {Array} marker - List of google.maps.Marker
 */
function createMarkers(){
  images = [
    '/img/icons/cat.png',
    '/img/icons/house.png',
    '/img/icons/tree.png',
    '/img/icons/bike.png',
    '/img/icons/car.png',
    '/img/icons/horse.png'
  ];
  markers = [];
  for(var i = 0; i<6;i++){
    image = {
      url: images[i]
    };
    markers.push(new google.maps.Marker({
      icon: image,
      clickable: false
    }));
  }
  return markers;
}
