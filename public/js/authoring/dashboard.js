$(function() {
  "use strict";
  var dist = 14 - downloadlist.length;
  var counter = 0;
  var updatelist = [];
  var creator = true; // this vble is used to identify if the user is the creator of the activity or not



    function initialize() {
        var myLatlng = new google.maps.LatLng(activity.data.point0.lat,activity.data.point0.lng);
        var myLatlng1 = new google.maps.LatLng(activity.data.point1.lat,activity.data.point1.lng);
        var myLatlng2 = new google.maps.LatLng(activity.data.point2.lat,activity.data.point2.lng);
        var myLatlng3 = new google.maps.LatLng(activity.data.point3.lat,activity.data.point3.lng);
        var myLatlng4 = new google.maps.LatLng(activity.data.point4.lat,activity.data.point4.lng);
        var myLatlng5 = new google.maps.LatLng(activity.data.point5.lat,activity.data.point5.lng);
        var mapOptions = {
            zoom: 17,
            center: myLatlng1,
            mapTypeId: google.maps.MapTypeId.SATELLITE,
            scrollwheel: false,
            scaleControl: false,
            draggable: false
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        var path;

        var rectangle = [
            myLatlng,
            myLatlng1,
            myLatlng2,
            myLatlng3,
            myLatlng4,
            myLatlng5
        ];

        path = new google.maps.Polygon({
            paths: rectangle,
            strokeColor: 'blue',
            strokeOpacity: 0.5,
            strokeWeight: 4,
            fillColor: '',
            fillOpacity: 0
        });

        path.setMap(map);


        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: "/img/icons/cat.png",
            title: 'Cat'
        });
        var marker1 = new google.maps.Marker({
            position: myLatlng1,
            map: map,
            icon: "/img/icons/house.png",
            title: 'House'
        });
        var marker2 = new google.maps.Marker({
            position: myLatlng2,
            map: map,
            icon: "/img/icons/tree.png",
            title: 'Tree'
        });
        var marker3 = new google.maps.Marker({
            position: myLatlng3,
            map: map,
            icon: "/img/icons/bike.png",
            title: 'Bike'
        });
        var marker4 = new google.maps.Marker({
            position: myLatlng4,
            map: map,
            icon: "/img/icons/car.png",
            title: 'Car'
        });
        var marker5 = new google.maps.Marker({
            position: myLatlng5,
            map: map,
            icon: "/img/icons/horse.png",
            title: 'Horse'
        });
    }

    //google.maps.event.addDomListener(window, 'load', initialize);
    initialize();

  
//socket.io 
  var socket = io.connect('http://trigo.lnu.se:80'); //
 // var socket = io('http://194.47.172.54:3232');
  socket.on('download', function (data) {
     console.log(activity);
     console.log("activity id received: " + data.activityID);
     if (data.activityID == activity._id){
	   	 //
	     var i = data.group;
	     var div = "<div style='margin-top: -7px; margin-bottom: -6px; margin-left: 14px; font-size: 11px; height:15px; width: 90px;'>"+data.date+"</div>" ;
	     $('#itemdo'+i).append(div);

         //change the class of the 'li' to make it blue
         $('#itemdo'+i).removeClass('list-group-item').addClass('list-group-item list-group-item-primary active');

         //check the unchecked checkbox
         $('#itemdo'+i).children('span').removeClass('glyphicon-unchecked').addClass('glyphicon-check');


     }//else
   	  //console.log(" not me");
  });
  
  socket.on('upload', function (data) {
	  console.log("uploaded file" + JSON.stringify(data));

      var i = data.group;
      var countFiles = data.countFiles;
      activity.uploaded_xmls.push('socket'); //This is only for the case that there is uploads but the website has not been reloaded.
      var div = "";
      if (countFiles == 0){
    	  div = "<div id='div"+i+"' style='margin-top: -3px; margin-left: 1px; font-size: 11px; font-weight: normal; padding-bottom: 0px; width: 90px;'>"+data.date+"</div>" ; 
    	  $('#itemup'+i).append(div);
      }
     if (countFiles > 0){
    	  $('#div'+i).html(data.date + " (" + (countFiles+1)+ ")");
      }      
   });
  console.log(user);
  console.log( "user.username: " + user._id + ", activity username: " + activity.user);
  
  if (user._id != activity.user){ // hidding buttons if user is not the creator of the activity
	  document.getElementById("buttons-container").style.visibility = "hidden";
	  document.getElementById("updatedlbt").style.visibility = "hidden";
      document.getElementById("questionnaire").style.visibility = "hidden";
      document.getElementById("code").style.visibility = "hidden";
      document.getElementById("password").style.visibility = "hidden";
      document.getElementById("teacherpassword").style.visibility = "hidden";
      for (var i=0; i<28; i++)
        document.getElementsByClassName("timestamp")[i].style.display = "none";
	 // var arrayList = document.getElementsByClassName("well")[0].children[1] ;// getting check boxes for download and disable them 
	 creator = false;
  }
    //console.log(activity.questionnaire);
    if (activity.questionnaire == true) {
        document.getElementById("questBtn").checked = true;
    } else {
        document.getElementById("questBtn").checked = false;
    }
;

   if (creator){ // if this activity belongs to the current user show the checkboxes otherwise ignore it
    	  $(".dllist").children('.list-group.checked-list-box .list-group-item').each(function () {
    		    // Settings
    		    var $widget = $(this),
    		      $checkbox = $('<input type="checkbox" class="hidden" />'),
    		      color = ($widget.data('color') ? $widget.data('color') : "primary"),
    		      style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
    		      settings = {
    		        on: {
    		          icon: 'glyphicon glyphicon-check'
    		        },
    		        off: {
    		          icon: 'glyphicon glyphicon-unchecked'
    		        }
    		      };

    		    counter++;

    		    $widget.on('click', function () {
    		      var isInList = false;
    		      var index = $(this).attr("id").split("itemdo");
    		      if (updatelist.length > 0){
    		        updatelist.forEach(function(item){
    		          if (item == parseInt(index[1])){
    		            isInList = true;
    		            updatelist.splice(updatelist.indexOf(item) ,1);
    		          }
    		        });
    		      } else { //if empty then add index
    		        isInList = true;
    		        updatelist.push(parseInt(index[1]));
    		      }

    		      if (isInList == false){
    		        updatelist.push(parseInt(index[1]));
    		      }

    		      $checkbox.prop('checked', !$checkbox.is(':checked'));
    		      $checkbox.triggerHandler('change');
    		      updateDisplay();
    		    });

    		    $checkbox.on('change', function () {
    		      updateDisplay();
    		    });

    		    if (downloadlist.indexOf(counter) == -1) {
    		      //console.log(counter);
    		      //console.log(downloadlist.indexOf(counter));
    		      $checkbox.prop('checked', !$checkbox.is(':checked'));
    		      $checkbox.triggerHandler('change');
    		      updateDisplay();
    		      $checkbox.on('change', function () {
    		        updateDisplay();
    		      });
    		    }

    		    if (counter <= dist){
    		      /*$checkbox.prop('checked', !$checkbox.is(':checked'));
    		      $checkbox.triggerHandler('change');
    		      updateDisplay();
    		      $checkbox.on('change', function () {
    		        updateDisplay();
    		      });*/
    		    }

    		    // Actions
    		    function updateDisplay() {
    		      var isChecked = $checkbox.is(':checked');

    		      // Set the button's state
    		      $widget.data('state', (isChecked) ? "on" : "off");

    		      // Set the button's icon
    		      $widget.find('.state-icon')
    		        .removeClass()
    		        .addClass('state-icon ' + settings[$widget.data('state')].icon);

    		      // Update the button's color
    		      if (isChecked) {
    		        $widget.addClass(style + color + ' active');
    		      } else {
    		        $widget.removeClass(style + color + ' active');
    		      }
    		      // Update the button's color
    		      /*if (counter <= dist) {
    		       $widget.addClass(style + color + ' active');
    		       $widget.data('state', (isChecked) ? "on" : "on");
    		       } else {
    		       $widget.removeClass(style + color + ' active');
    		       }*/
    		    }

    		    // Initialization
    		    function init() {
    		      if ($widget.data('checked') == true) {
    		        $checkbox.prop('checked', !$checkbox.is(':checked'));
    		      }



    		      updateDisplay();

    		      // Inject the icon if applicable
    		      if ($widget.find('.state-icon').length == 0) {
    		        $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
    		      }

    		    }
    		        
    		    init();
    		  });
    } else
        console.log("The user is not the creator of the activity");


    //console.log(activity);
    if(activity.location == null) {
        //location is not defined. Call Google Maps API to find it
        var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + activity.data.point0.lat + ',' + activity.data.point0.lng;
        $.get(url, function (data) {
            document.getElementById('City').innerHTML = '- ' + data.results[0]['formatted_address'];
            console.log(data.results[0]['formatted_address']);
            //save the location in the database if it is still not defined.
            activity.location = data.results[0]['formatted_address'];
        });
    } else {
        //location is already saved in the database
        console.log(activity.location);
        document.getElementById('City').innerHTML = activity.location;
    }

  // =

  $("#reset-activity").click(function(){
    $('#myModal').modal("show");
  });

  $("#download-kml").click(function(){
    if (activity.uploaded_xmls.length != 0) {
        var selected = $("#kml-select").val();
        //var r = confirm("Do you want to download the KML file?\n-> id: " + activity._id);
        //if (r == true) {
            window.location = "/api/getKML/" + activity._id + "/" + selected;
        //} else {
        //    window.location = "/visualizing/" + activity._id + "/" + selected + "/" + activity.title;
        //}

        /*$.get("/api/getKML/"+activity._id+"/"+selected, function(data, status, xhr) {
         window.location.href = data.URL
         var iframe = document.createElement("iframe");
         iframe.setAttribute("src", data.URL);
         iframe.setAttribute("style", "display: none");
         document.body.appendChild(iframe);
         });*/
    } else {
        alert ('There are no uploaded files. Students have to upload their activities before generating the KML file');
    }
  });
  $("#generate-visualization").click(function(){
    if (activity.uploaded_xmls.length != 0) {
        var selected = $("#kml-select").val();
        //var r = confirm("Do you want to download the KML file?\n-> id: " + activity._id);
        //if (r == true) {
        //    window.location = "/api/getKML/" + activity._id + "/" + selected;
        //} else {
            window.location = "/visualizing/" + activity._id + "/" + selected + "/" + activity.title;
        //}

        /*$.get("/api/getKML/"+activity._id+"/"+selected, function(data, status, xhr) {
         window.location.href = data.URL
         var iframe = document.createElement("iframe");
         iframe.setAttribute("src", data.URL);
         iframe.setAttribute("style", "display: none");
         document.body.appendChild(iframe);
         });*/
    } else {
        alert ('There are no uploaded files. Students have to upload their activities before generating the KML file');
    }
  });
  $("#updatedlbt").click(function(){
    $('#updateModal').modal("show");
  });

  $("#reset-button").click(function(){
    window.location.href = "/authoring/reset/"+activity._id;
  });

  $("#questBtn").click(function(){
    var checked = document.getElementById("questBtn").checked;
    activity.questionnaire = checked;
    $.post("/api/updateQuestionnaireState/" + activity._id + "/"+checked, function( data ) {  //g
        console.log(data);
    }, "json");

  });

  $("#update-dl-button").click(function(){
    var code = $("#code").text(),
        pwd = $("#password").text(),
        checkedlist = [],
        uncheckedlist = [1,2,3,4,5,6,7,8,9,10,11,12,13,14];
    console.log($(".list-group-item-primary"));
    console.log($(".dllist").children(".list-group-item-primary"))
    $(".dllist").children(".list-group-item-primary").each(function(){
      var c = $(this).attr("id").split("item");
      console.log(uncheckedlist.indexOf(parseInt(c[1])));
      if (uncheckedlist.indexOf(parseInt(c[1])) != -1){
        var index = uncheckedlist.indexOf(parseInt(c[1]));
        uncheckedlist.splice(index, 1);
      }
    });
    console.log(uncheckedlist);
    var data = {}
    data.xmllist = uncheckedlist;
    console.log(updatelist);
    data.updatelist = updatelist;





    $.post("/api/updateXML/"+code+";"+pwd, data , function( data ) {

    }, "json");
    $('#updateModal').modal("toggle");
  });


  
});