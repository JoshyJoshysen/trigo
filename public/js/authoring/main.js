
$(function() {
  "use strict";

  /**** INDEX   -------------------------------------------------------

   - VARIABLES
   - FUNCTONS
   - saveScenario(): post scenario to backend
   - createScenario(): create scenario array
   - addElement(): adding Elements to the Scenario
   - droppable(): drop functionality
   - sortable(): make it possible to rearrange elements
   - addScreen(): add new screen to the scenario
   - updateScreenNUmber(): update screen number when screens are sorted
   - EVENT HANDLERS / TRIGGERS
   - handler for add new screen
   - delete element button handler
   - handle button for additional choice
   - show delete-screen-button on hover over screen-number
   - hide delete-screen-button on mouseleave of screen-number
   - delete screen when delete-screen-button is clicked
   - INITIALIZATION
   - initialize dragging of different buttons
   - UI container (tabs) initialize

   --------------------------------------------------------------------   ****/


  /* VARIABLES
   ------------------------------------------------*/
  var is_dragging = false;
  var screenNumber = 0;
  var screenID = '';
  var choiceCount = 2;
  var element = '';
  var instruction = '';
  var textField = '<input type="text" disabled="disabled"/>';
  var textArea = '<textarea disabled="disabled"></textarea>';
  var image = '<button disabled="disabled">Take a Snapshot</button>';
  var video = '<button disabled="disabled">Make a Video</button>';
  var sound = '<button disabled="disabled">Start Recording</button><p class="bg-warning">Attention: Not supported by iOS devices!</p>';
  //var choice = '<div class="choices"><input class="1" type="radio" name="choice" value="option1" disabled="disabled"><input class="label" type="text" placeholder="Option 1"/><br><input class="2" type="radio" name="choice" value="option2" disabled="disabled"><input class="label" type="text" placeholder="Option 2"/><br>';
  var option = '<div><input type="checkbox" name="choice"><input class="label" type="text" placeholder="Option"/><span class="icon-trash"></span></div>';
  var choice = '<div class="choices">' + option + option + '</div><div id="add-choice" class="button grey"><div class="icon icon-plus"></div>Add another choice</div>';
  var numerical = '<input type="number" disabled="disabled"/>';
  var date = '<input type="date" disabled="disabled"/>';
  var location = '<button disabled="disabled">Add Current Location</button>';
  var userID = $("#user").attr("uid");


  /* FUNCTIONS
   ------------------------------------------------*/


  /*
   function checkPasswords(){
   console.log('checkPasswords');
   var pass1 = $("#reg_password").val();
   var pass2 = $("#reg_password2").val();
   console.log('checkPasswords'+pass1);
   console.log('checkPasswords'+pass2);

   if(pass1 == pass2)
   {
   $(":text").removeClass("incorrect");
   console.log('submitting form');
   document.forms["reg_form"].submit();
   }

   else
   {
   $(":text").addClass("incorrect");
   alert("Passwords not equal");
   }
   console.log('checkPasswords');
   };
   */

  $(".nav-item").droppable({
    over: function( event, ui ) {
      var targetId = $(event.target).attr('id').replace('nav', '');
      $("a[href=#screen"+ targetId + "]").trigger('click');
    }
  });

  //make existing screens sortable
  $(".screen").sortable({
    connectWith: ".screen",
    cursor: "move",
    start: function(event, ui) {
      is_dragging = true;
    },
    stop: function(event, ui) {
      //console.log("reorder screen");
      ga('send', 'event', 'user_action', 'reorder_screen', userID);
      is_dragging = false;
    }
  }).disableSelection().droppable({
      drop: function( event, ui ) {
        var elementName = ui.draggable[0].classList[0];
        if(!$(ui.draggable[0]).hasClass('dropped')){
          switch (elementName){
            case 'instruction':
              element = instruction;
              break;
            case 'textfield':
              element = textField;
              break;
            case 'textarea':
              element = textArea;
              break;
            case 'image':
              element = image;
              break;
            case 'video':
              element = video;
              break;
            case 'sound':
              element = sound;
              break;
            case 'choice':
              element = choice;
              break;
            case 'numerical':
              element = numerical;
              break;
            case 'date':
              element = date;
              break;
            case 'location':
              element = location;
              break;
            default:
              break;
          }
          addElement(this, element, elementName);
        }
      }
    }
  );

  $("#default-fields").droppable("destroy").sortable("destroy");


  //posting to backend
  function saveScenario(scenario){
    ga('send', 'event', 'user_action', 'save_scenario', userID);
    sendScenarioData(scenario);
  }





  //adding Elements to the Scenario
  function addElement(targetScreen, element, elementName){
    if (elementName === 'instruction') { // instrucion elements don't have the required option
      $(targetScreen).append('<div class="scenario-element dropped '+ elementName +'"><textarea class="label" type="text" placeholder="Instruction"/></div>').find('textarea.label').focus().autosize();
    } else if (elementName === 'sound') { // sound elements have the required checkbox disabled since it is not working on iOS
      $(targetScreen).append('<div class="scenario-element dropped '+ elementName +'"><textarea class="label" type="text" placeholder="Label"/></br><div class="content required">' + element + '</div><div class="required-switch"><input type="checkbox" name="required" value="false" disabled><span>Required</span></div></div>').find('textarea.label').focus().autosize();
    } else {
      $(targetScreen).append('<div class="scenario-element dropped '+ elementName +'"><textarea class="label" type="text" placeholder="Label"/></br><div class="content required">' + element + '</div><div class="required-switch"><input type="checkbox" name="required" value="false"><span>Required</span></div></div>').find('textarea.label').focus().autosize();
    }
  };

  //drop functionality
  function droppable(screenID){
    //allow dropping new elements into the scenario
    $("#screen" + screenID).droppable({
      drop: function( event, ui ) {
        ga('send', 'event', 'user_action', 'add_element', userID);
        var elementName = ui.draggable[0].classList[0];
        if(!$(ui.draggable[0]).hasClass('dropped')){
          switch (elementName){
            case 'instruction':
              element = instruction;
              break;
            case 'textfield':
              element = textField;
              break;
            case 'textarea':
              element = textArea;
              break;
            case 'image':
              element = image;
              break;
            case 'video':
              element = video;
              break;
            case 'sound':
              element = sound;
              break;
            case 'choice':
              element = choice;
              break;
            case 'numerical':
              element = numerical;
              break;
            case 'date':
              element = date;
              break;
            case 'location':
              element = location;
              break;
            default:
              break;
          }
          ga('send', 'event', 'user_action', 'add_element_'+elementName, userID);
          addElement(this, element, elementName);
        }
      }
    });
  };



  //make it possible to rearrange elements
  function sortable(screenID) {
    $("#screen" + screenID).sortable({
      connectWith: ".screen",
      cursor: "move",
      start: function(event, ui) {
        is_dragging = true;
      },
      stop: function(event, ui) {
        is_dragging = false;
        ga('send', 'event', 'user_action', 'reorder_screen', userID);
      }
    }).disableSelection();
  };

  //add new screen to the scenario
  function addScreen() {
    screenNumber = $('.block').length+1;
    $("#screen-nav").append('<div id="nav'+screenNumber+'" class="dropped nav-item"><span class="icon-mobile-1"></span><a href="#screen'+ screenNumber +'">Screen '+ screenNumber +'</a><span class="icon-cancel"></span></div>');
    $("a[href=#screen"+ screenNumber + "]").trigger('click');
    $("#screens").append("<div id='screen"+ screenNumber +"' class='screen block droppable active'></div>");

    $("#nav" + screenNumber).droppable({
      over: function( event, ui ) {
        var targetId = $(event.target).attr('id').replace('nav', '');
        $("a[href=#screen"+ targetId + "]").trigger('click');
      }
    });
    droppable(screenNumber);
    sortable(screenNumber);
    updateScreenNumber();
    ga('send', 'event', 'user_action', 'add_screen', userID);
  };

  //update screen number when screens are sorted
  function updateScreenNumber(){
    $('#screen-nav > .nav-item').each(function() {
      var i = $(this).index() + 1;
      $(this).find('a').html('Screen ' + i);
    });
  };


  /* EVENT HANDLING / TRIGGERS
   ------------------------------------------------*/
  $(document).on('click', '.icon.icon-cancel', function(e){
    e.stopPropagation();
    var scenarioItem = $(this).parent().parent();
    var id = scenarioItem.attr('id');
    //console.log(id);

    $.ajax({
      type: "GET",
      url: '/deletescenario?scenario_id=' + id,
      success: function(data){
        //console.log('scenario deleted');
        scenarioItem.remove();
      }
    });

  });

  $(document).on('click', '.nav-item .icon-cancel', function(event){
    event.stopImmediatePropagation();
    var id = $(this).parent().attr('id').replace('nav', '');
    $('#screen' + id).remove();
    $(this).parent().remove();
    var prev = parseInt(id);
    var prev_int = prev - 1;
    var newId = prev_int.toString();
    //console.log(newId);
    if (newId > 0) {
      $('.nav-item').removeClass('active');
      $('#nav' + newId).addClass('active');
      $('.screen').removeClass('active');
      $('#screen' + newId).addClass('active');
    } else {
      $('.nav-item').removeClass('active');
      $('#information .nav-item').addClass('active');
      $('#delete-element').hide();
      $('#element-panel').hide();
      $('.screen').removeClass('active');
      $('#default-fields').addClass('active');
    }

    updateScreenNumber();

    ga('send', 'event', 'user_action', 'delete_screen', userID);
  });

  $('#information .nav-item').click(function(){
    $('.nav-item').removeClass('active');
    $(this).addClass('active');

    $('#delete-element').hide();
    $('#element-panel').hide();
    $('.screen').removeClass('active');
    $('#default-fields').addClass('active');
  });

  $(document).on('click', '.nav-item', function(){
    $('.nav-item').removeClass('active');
    $(this).addClass('active');
  });

  $(document).on('click', '#screen-nav .nav-item', function(){
    $('#delete-element').show();
    $('#element-panel').show();

    $('.screen').removeClass('active');
    var id = $(this).attr('id').replace('nav', '');
    $('#screen' + id).addClass('active');

    $('#screen' + id).find('textarea.label').trigger('autosize.resize');
  });

  $(document).on('click', '.label, #scenario-description', function() {
    $(this).focus();
  })

  $('#register form button').click(function(event){
    event.preventDefault;
    checkPasswords();
  });

  $('#login form button').click(function(event){
    event.preventDefault;
    console.log('before post');
    $.ajax({
      type: "POST",
      url: '/login',
      data: data,
      error: function(data){
        console.log('error');
      }
    });
    console.log('after post');

  });

  //handler for add new screen
  $("div#add-screen").click(function(){
    addScreen();
  });

  //delete element button handler
  $("#delete-element").droppable({
    hoverClass: "active",
    drop: function( event, ui ) {
      ga('send', 'event', 'user_action', 'delete_element', userID);
      $(ui.draggable[0]).remove();
    }
  });

  //handle button for additional choice
  $(document).on('keydown', '.choices input', function(event) {
    if ( event.which == 13 ) {
      event.preventDefault();
      choiceCount += 1;
      $(this).parent().append('<input class="'+ choiceCount +'" type="radio" name="choice" value="option'+ choiceCount +'" disabled="disabled"><input class="label" type="text" placeholder="Option '+ choiceCount +'"/><br>');
      $(this).parent().find('input:last').focus();
      $(this).next('input.label').focus();
    }
    else if(event.which == 8 && choiceCount > 1 && $(this).val() === ""){
      event.preventDefault();
      choiceCount -= 1;
      $(this).prev('input').remove();
      $(this).prev('br').remove();
      $(this).prev('input').focus();
      $(this).remove();
    }
  });

  //show delete-screen-button on hover over screen-number
  // $(document).on('mouseenter', 'li.screen-link', function(){
  //     screenID = $(this).attr('href');
  //     $(this).find('span').addClass('active');
  // });

  //hide delete-screen-button on mouseleave of screen-number
  // $(document).on('mouseleave', 'li.screen-link', function(){
  //     $(this).find('span').removeClass('active');
  // });

  //delete screen when delete-screen-button is clicked
  // $(document).on('click', 'span.delete-screen', function(){
  //     $(this).parent().remove();
  //     tabs.tabs('refresh');
  //     updateScreenNumber();
  // });

  $(document).on('click', '.scenario', function(){
    var id = $(this).attr('id');
    window.location = "editor.html?id=" + id;
  });



  // $(document).on('change','.required-switch input',function(){
  //     if ($(this).is(':checked')) {
  //         $(this).prevAll('.content').addClass('required');
  //     }
  //     else{
  //         $(this).prevAll('.content').removeClass('required');
  //     }
  // });


  $(document).on('click', '#add-choice', (function() {
    $(this).siblings('.choices').append(option);
  }));

  $(document).on('click', '.choices .icon-trash', (function() {
    var $parent = $(this).parent('div');
    if ($parent.siblings('div').length >= 2) {
      $parent.remove();
    } else {
      alert('Option cannot be removed. You need at least 2 options.');
    }
  }));

  /* INITIALIZE
   ------------------------------------------------*/

  //initialize dragging of different buttons
  $(".draggable").draggable({
    opacity: 0.8,
    revert: true,
    revertDuration: 0
  });

  $('#screen-nav').sortable({
    stop: function() {
      updateScreenNumber();
    }
  });

  droppable(1);
  sortable(1);

  //$("#default-fields").validate();

  $("#cancel").click(function(){
    ga('send', 'event', 'user_action', 'cancel', userID);
  });

});