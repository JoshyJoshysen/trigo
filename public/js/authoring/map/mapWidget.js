/**
 *	This class handles the communication between Google
 *	Maps API. This class still needs
 *	more work, but it's an ok start.
 *
 *	@author		Henrik Andersen <henrik.andersen@lnu.se>, Kevin Johne <kj222ic@student.lnu.se>
 *	@copyright	Copyright (c) 2015.
 *	@license	Creative Commons (BY-NC-SA)
 *	@version	1.0.
 *	@since		2013-11-11
 */
function MapWidget() {

  var _map;
  var _mapElement = document.getElementById("map");

  var DEFAULT_MAP_ZOOM = 17;
  var DEFAULT_MAP_LAT = 56.8770413;
  var DEFAULT_MAP_LNG = 14.8092744;

  var input = /** @type {HTMLInputElement} */(
    document.getElementById('pac-input'));

  /**
   *	The class constructor.
   *	@return {undefined}
   */
  function init() {
    initMap();

    var tileLoadedListener = google.maps.event.addListener(_map, 'tilesloaded', function() {
      //this.addTennisCourt();
      google.maps.event.trigger(_map,'resize');
      google.maps.event.removeListener(tileLoadedListener);
     // google.maps.event.clearListeners(_map, 'dblclick');
    });
  }

  /**
   *	Creates and initializes the map object.
   *	@return {undefined}
   */
  function initMap() {
    _map = new google.maps.Map(_mapElement, getDefaultMapSettings());
    _map.setCenter(new google.maps.LatLng(DEFAULT_MAP_LAT,DEFAULT_MAP_LNG));


    var searchBox = new google.maps.places.SearchBox(
      /** @type {HTMLInputElement} */(input));

    // [START region_getplaces]
    // Listen for the event fired when the user selects an item from the
    // pick list. Retrieve the matching places for that item.
    var markers = [];

    google.maps.event.addListener(searchBox, 'places_changed', function() {
      var places = searchBox.getPlaces();
      if (places.length == 0) {
        return;
      }
      var bounds = new google.maps.LatLngBounds();
      for (var i = 0, place; place = places[i]; i++) {
        bounds.extend(place.geometry.location);
      }
      _map.fitBounds(bounds);
      console.log(_map.zoom);
      if (_map.zoom >= 17){
        _map.setZoom(17);
      }
    });
    // [END region_getplaces]

    // Bias the SearchBox results towards places that are within the bounds of the
    // current map's viewport.
    google.maps.event.addListener(_map, 'bounds_changed', function() {
      var bounds = _map.getBounds();
      searchBox.setBounds(bounds);
    });

    google.maps.event.addListener(_map, 'zoom_changed', function() {
      //console.log(_map.zoom);
    });




    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(
        function success(position){
          _map.panTo(new google.maps.LatLng(position.coords.latitude,position.coords.longitude));
        },function error(error){
            console.log("getGeolocation error: " + error);
      });
    }else{
      error = "no navigation geolocation supported";
      getCurrentPositionError(error);
    }
  }

  MapWidget.prototype.getMap = function(){
    return _map;
  };

  /**   This method is meant to set the map settings.
   *	@return {Object}
   */
  function getDefaultMapSettings() {
    return {
      zoom: DEFAULT_MAP_ZOOM,
      MapTypeId: google.maps.MapTypeId.ROADMAP,
      zoomControl: true,
      scaleControl: true,
      disableDoubleClickZoom: true
    };
  }




  init();
}
