/**
 * Created by aaml on 24.02.2015.
 */

function TennisCourt() {
  //tennisCourt
  // 0 --- 1 ---- 2
  // |            |
  // 5 --- 4 ---- 3
  // [bering,distance]
  var polygonRelativeBounds = [
    [90, 35],
    [90, 35],
    [180, 28],
    [270, 35],
    [270, 35]
  ];

  var markers;
  var polygon;
  var map;
  var rotateHandler;
  var oldPhi = 0;

  TennisCourt.prototype.init = function init(startPoint) {
    tc = this;

    if (startPoint == undefined || !(startPoint instanceof google.maps.LatLng))
      startPoint = new google.maps.LatLng(0, 0);
    polygon = calculateField(startPoint);

    google.maps.event.addListener(polygon, 'drag', function(){
      tc.updateMarkers();
      tc.updateRotateHandler();
    });

/*
      var arr=[];
      polygon.getPath().forEach(function(latLng){arr.push(latLng.toString());})

      var res = arr[4].replace("(","");
      res = res.replace(")","");
      res = res.split(", ");

      var lat = parseFloat(res[0]) + parseFloat('0.0007');
      var lon = parseFloat(res[1]) + parseFloat('0.0000');)
*/
    options = {
      position: polygon.getCenter(),
      draggable: true,
      icon:"/img/icons/rotate.png",
      zIndex:99999999
    };
    rotateHandler = new google.maps.Marker(options);


    google.maps.event.addListener(rotateHandler, 'drag',function rotate(){
      if (map.zoom > 15){
        rotateHandler.setOptions({clickable:false});
        phi = tc.calculateAngel(this);
        var origin = polygon.getCenter();
        polygon.rotate(phi, origin);
        tc.updateMarkers();
      } else {
        rotateHandler.setOptions({clickable:false});
      }
    });

    google.maps.event.addListener(rotateHandler,'dragend',function resetMarker(){
      rotateHandler.setPosition(polygon.getCenter());
      oldPhi = 0;
    });
  };



  TennisCourt.prototype.calculateAngel = function(e){
    difference = polygon.getCenter().lng() - e.position.lng();
    phi = difference * 180/0.00057;
    tmpPhi = phi;
    phi = phi-oldPhi;
    oldPhi = tmpPhi;
    return phi;
  };

  /**
   *
   * @param {google.maps.Map} m
   */
  TennisCourt.prototype.setMap = function (m) {
    if (m instanceof google.maps.Map) {
      map = m;

      polygon != undefined?
        polygon.setMap(m):null;

      markers != undefined?
        markers.forEach(function(marker,point){
          marker.setMap(m);
        }):null;

      rotateHandler.setMap(m);
    } else {
      console.log("no instance of google.maps.Map");
    }
  };

  /** @ToDO work on setCenter method
  TennisCourt.prototype.setCenter = function (LatLng) {
    polygon.setCenter(LatLng);
    this.updateMarkers();
  };
   */

  TennisCourt.prototype.getPolygon = function(){
    return polygon;
  };

  TennisCourt.prototype.getRotateHandler = function(){
    return rotateHandler;
  };


  TennisCourt.prototype.removeRotateIcon = function(){
    rotateHandler.setMap(null);
  };

  /*
  TennisCourt.prototype.clearMarkers = function(){
    markers = [];
    this.updateMarkers();
  };
  */

  /**
   * @param {google.maps.LatLng} startPoint
   * @return {google.maps.Polygon}
   */
  function calculateField(startPoint) {
    var points = [];
    points.push(startPoint);

    for (var i = 0; i < polygonRelativeBounds.length; i++) {
      var x_y = destinationPoint(
        points[i].lat().toRadians(),
        points[i].lng().toRadians(),
        polygonRelativeBounds[i][1],
        polygonRelativeBounds[i][0]
      );

      var latLng = new google.maps.LatLng(
        x_y[0].toDegrees(),
        x_y[1].toDegrees()
      );
      points.push(latLng);
    }

    var options = {
      path: points,
      draggable: true,
      strokeColor: '#000',
      fillColor: '#fff',
      fillOpacity: '0.3'
    };

    return new google.maps.Polygon(options);
  }

  /**
   * set Marker to the points of an Polygon
   * Length of array MUST as big as polygon
   * @param {Array} m - List of markers
   */
  TennisCourt.prototype.setMarkers = function (m) {
    var polygonPoints = polygon.getPath().getArray();
    if (m.length == polygonPoints.length) {
      m.forEach(function (marker, index) {
        marker.setMap(map);
      });
      markers = m;
      this.updateMarkers();
    } else if (m.length == 0) {
      markers.forEach(function (mk, index) {
        mk.setMap(null);
      });
      markers = m;
      //this.updateMarkers();
    }else {
      console.log("Array of Markers are shorter or longer then Polygon points")
      console.log(m.length);
    }
  };

  /**
   * update the Marker position of the Polygon
   */
  TennisCourt.prototype.updateMarkers = function () {
    polygon.getPath().forEach(function (point, index) {
      markers[index].setPosition(point);
    });
  };

  /**
   * Update position of Handler for rotation
   */
  TennisCourt.prototype.updateRotateHandler = function(){
    rotateHandler.setPosition(polygon.getCenter());
  };

  /**
   * Returns the destination point from 'this' point having travelled the given distance on the
   * given initial bearing (bearing normally varies around path followed).
   *
   * @param   {number} φ1 - latitude of Point in Radians
   * @param   {number} λ1 - longitude of Point in Radians
   * @param   {number} distance - Distance travelled, in same units as earth radius (default: metres).
   * @param   {number} bearing - Initial bearing in degrees from north.
   * @param   {number} [radius=6371e3] - (Mean) radius of earth (defaults to radius in metres).
   */
  function destinationPoint(φ1, λ1, distance, bearing, radius) {
    // see http://williams.best.vwh.net/avform.htm#LL
    if (radius === undefined) radius = 6371e3;

    var δ = Number(distance) / radius; // angular distance in radians
    var θ = Number(bearing).toRadians();

    var φ2 = Math.asin(Math.sin(φ1) * Math.cos(δ) +
    Math.cos(φ1) * Math.sin(δ) * Math.cos(θ));
    var λ2 = λ1 + Math.atan2(Math.sin(θ) * Math.sin(δ) * Math.cos(φ1),
        Math.cos(δ) - Math.sin(φ1) * Math.sin(φ2));
    λ2 = (λ2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI; // normalise to -180..+180°

    return [φ2, λ2];
  }

  /** Extend Number object with method to convert numeric degrees to radians */
  if (Number.prototype.toRadians === undefined) {
    Number.prototype.toRadians = function () {
      return this * Math.PI / 180;
    };
  }

  /** Extend Number object with method to convert radians to numeric (signed) degrees */
  if (Number.prototype.toDegrees === undefined) {
    Number.prototype.toDegrees = function () {
      return this * 180 / Math.PI;
    };
  }
}