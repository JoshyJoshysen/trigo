<?php
// KML definition file inclusion
include_once('kml.class.php');

//Global variables
//$path="/Applications/MAMP/htdocs/test";
//$path="/home/jazbaa/develop/trigo/dep/public/files";
$path="/home/jazbaa/develop/trigo/test/public/files";
$start_time=getrusage();
$activity;
$selection;
$xmlFiles;


function rutime($ru, $rus, $index) {
    return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
     -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
}

//Get the parameters given in the command line
parse_str(implode('&', array_slice($argv, 1)), $_GET);

//Define the group name
if(!isset($_GET['activity']))  {
	die("Arguments error\n"); 
}else{
	$activity = $_GET['activity'];
	$fileName = $activity.".kml";
	echo "OUTPUT=".$fileName."\n";
}

if(!isset($_GET['select']))  {
	$selection="all"; 
}else{
	$selection = $_GET['select'];
}

//Check the number of XML input files
//$fi = new FilesystemIterator("/home/jazbaa/develop/trigo/dep/public/files/".$activity."/uploads", FilesystemIterator::SKIP_DOTS);
$fi = new FilesystemIterator($path."/".$activity."/uploads", FilesystemIterator::SKIP_DOTS);
//printf("There are %d Files for the activity \"%s\"\n", iterator_count($fi), $activity);

//Iterate for each one of the Input XML files, sorting them by Group Name

$kml = new KML($fileName);

$document = new KMLDocument($fileName, $activity);

// Style definitions and loading image files
$style = new KMLStyle('1');
$style->setIconStyle('images/cat.jpg', 'ffffffff', 'normal', 1);
$style->setLineStyle('ffffffff', 'normal', 2);
$document->addStyle($style);
$kml->addFile('images/cat.jpg', 'images/cat.jpg');

$style = new KMLStyle('6');
$style->setIconStyle('images/horse.jpg', 'ffffffff', 'normal', 1);
$style->setLineStyle('ffffffff', 'normal', 2);
$document->addStyle($style);
$kml->addFile('images/horse.jpg', 'images/horse.jpg');

$style = new KMLStyle('3');
$style->setIconStyle('images/tree.jpg', 'ffffffff', 'normal', 1);
$style->setLineStyle('ffffffff', 'normal', 2);
$document->addStyle($style);
$kml->addFile('images/tree.jpg', 'images/tree.jpg');

$style = new KMLStyle('2');
$style->setIconStyle('images/house.jpg', 'ffffffff', 'normal', 1);
$style->setLineStyle('ffffffff', 'normal', 2);
$document->addStyle($style);
$kml->addFile('images/house.jpg', 'images/house.jpg');

$style = new KMLStyle('4');
$style->setIconStyle('images/bike.jpg', 'ffffffff', 'normal', 1);
$style->setLineStyle('ffffffff', 'normal', 2);
$document->addStyle($style);

$style = new KMLStyle('5');
$style->setIconStyle('images/car.jpg', 'ffffffff', 'normal', 1);
$style->setLineStyle('ffffffff', 'normal', 2);
$document->addStyle($style);
$kml->addFile('images/car.jpg', 'images/car.jpg');

$style = new KMLStyle('pending');
$style->setIconStyle('images/pending.jpg', 'ffffffff', 'normal', 1);
$style->setLineStyle('ffffffff', 'normal', 2);
$document->addStyle($style);
$kml->addFile('images/pending.jpg', 'images/pending.jpg');

$style = new KMLStyle('ok');
$style->setIconStyle('images/ok.png', 'ffffffff', 'normal', 1);
$style->setLineStyle('ffffffff', 'normal', 2);
$document->addStyle($style);
$kml->addFile('images/ok.png', 'images/ok.png');

$style = new KMLStyle('rejected');
$style->setIconStyle('images/rejected.png', 'ffffffff', 'normal', 1);
$style->setLineStyle('ffffffff', 'normal', 2);
$document->addStyle($style);
$kml->addFile('images/rejected.png', 'images/rejected.png');

for($i=1; $i<=10; $i++){
	$style = new KMLStyle('point'.$i);
	$style->setIconStyle('images/'.$i.'-lv.png', 'ffffffff', 'normal', 0.4);
	$style->setLineStyle('ffffffff', 'normal', 2);
	$document->addStyle($style);
	$kml->addFile('images/'.$i.'-lv.png', 'images/'.$i.'-lv.png');
}


//determine the XML files to be processed
$filecounter=0;
foreach($fi as $fileinfo){
	$filename = $fileinfo->getFilename();
	$groupname = explode('_',$filename);
	if ($filecounter==0){
		$xmlFiles[$filecounter++] = $filename;
	}else{
		if ($selection == "last"){  //Process only the fast XML from each group
			foreach($xmlFiles as $key=>$f0){
				$g0 = explode('_',$f0);
				if(strcmp($g0[0], $groupname[0])==0){
					$found = True;
					$xmlFiles[$key] = $filename;
					break;
				}else{
					$found = False;
				}
			}
			if(!$found){
				$xmlFiles[$filecounter++] = $filename;
			}
		}else{
			if($selection == "first"){  //Process only the first XML from each group
				foreach($xmlFiles as $f0){
					$g0 = explode('_',$f0);
					if(strcmp($g0[0], $groupname[0])==0){
						$found = True;
						break;
					}else{
						$found = False;
					}		
				}
				if(!$found){
					$xmlFiles[$filecounter++] = $filename;
				}
			}else{	//Process all the XML files
				$xmlFiles[$filecounter++] = $filename;
			}
		}
	}
}

//Prepare the list of reference points. We can use any of the XML Log files for this part, as the header is the same for all
foreach($xmlFiles as $filename){
}	
$xmldoc = simplexml_load_file($path."/".$activity."/uploads/".$filename);

foreach($xmldoc as $activities){
	//loading the reference points that have been used
	$reference_points;
	foreach($activities->point as $ref_point){
		$pos=		$ref_point['id'];
		global $reference_points;
		$reference_points[$pos-1][0]=$ref_point->img;
		$reference_points[$pos-1][1]=$ref_point->lat;
		$reference_points[$pos-1][2]=$ref_point->lon;
	}
}

//iterate each XML file (that needs to be processed) from the groups
foreach($xmlFiles as $filename){
	$groupname = explode('_',$filename);
	$kml_group = new KMLFolder('', $groupname[0]);
	$xmldoc = simplexml_load_file($path."/".$activity."/uploads/".$filename);
	foreach($xmldoc as $activities){
	}

	foreach($activities->task as $task){
		$taskId = $task['id'];
		$kml_task = new KMLFolder('', "Task ".$taskId);
		$con = 1;
		foreach($task->point->distance as $distance){
			$referenceID = $distance['ref'];
			$lat = $reference_points[$referenceID-1][1];
			$lon = $reference_points[$referenceID-1][2];
		
			$kml_reference = new KMLPlaceMark('Punkt-'.$con.' '.$distance, 'Punkt-'.$con.' '.$distance, '', true);
			$kml_reference->setGeometry(new KMLPoint($lon, $lat, 0));
			$kml_reference->setStyleUrl('#'.$referenceID);
			$kml_task->addFeature($kml_reference);
			$con++;
		}
		foreach($task->attempts->attempt as $attempt){
			$attemptID = $attempt['id'];
			$lat = $attempt->lat;
			$lon = $attempt->lon;
			$dist1= $attempt->dist1;
			$dist2= $attempt->dist2;
			switch($attempt->status){
				case "ok":
					$kml_reference = new KMLPlaceMark($dist1.",".$dist2, $dist1.",".$dist2, '', true);
					$kml_reference->setGeometry(new KMLPoint($lon, $lat, 0));
					$kml_reference->setStyleUrl('#ok');
					$kml_task->addFeature($kml_reference);
					break;
				case "rejected":
					$kml_reference = new KMLPlaceMark($dist1.",".$dist2,$dist1.",".$dist2, '', true);
					$kml_reference->setGeometry(new KMLPoint($lon, $lat, 0));
					$kml_reference->setStyleUrl('#rejected');
					$kml_task->addFeature($kml_reference);
					break;
				default:
					$kml_reference = new KMLPlaceMark('',$dist1.",".$dist2, '', true);
					$kml_reference->setGeometry(new KMLPoint($lon, $lat, 0));
					$kml_reference->setStyleUrl('#point'.$attemptID);
					$kml_task->addFeature($kml_reference);
					break;
			}
		}
		$kml_group->addFeature($kml_task);
	}
	$document->addFeature($kml_group);
}
//	$kml_activity->addFeature($kml_group);

// Generate the KML structure
$kml->setFeature($document);
$kml->output('F', $path."/$activity/".$fileName);

$final_time = getrusage();
echo "Time consumed: ".rutime($final_time, $start_time, "utime")."ms for the KML generation\n";

?>
