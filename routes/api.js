var express = require('express');
var router = express.Router();
var trigo = require('../app');
var fse = require('fs-extra');
var exec = require("child_process").exec;


/* GET the JSON of an activity. */
router.get('/getActivity/:aid?', function(req, res, next) {
  var Activity = trigo.getActivity();
  var aId = req.params.aid;
  Activity.findById(aId, function(err, activity){
    res.json(activity);
  });
});

/* GET all ownActivities . */
router.get('/getAllActivities/', function(req, res, next) {
	
  var Activity = trigo.getActivity();
  var aId = req.params.aid;
  Activity.find(function(err, activities){
    res.json(activities);
  });
});

/* GET user by ID . */
router.get('/getUserById/:uid?', function(req, res, next) {
  var User = trigo.getUser();
  var uId = req.params.uid;
  User.findById(uId, function(err, user){
    if (err){
      console.error(err);
      res(err);
    } else {
      res.json(user);
    }
  });
});

/* update the state of the questionnaire for an activity */
router.post('/updateQuestionnaireState/:aid?/:check?', function(req, res, next) {
    var Activity = trigo.getActivity();
    var aId = req.params.aid;
    var checked = req.params.check;
    Activity.findById(aId, function (err, activity) {
        activity.questionnaire = checked;
        console.log(aId + ' -> ' + activity.questionnaire);
        activity.save(function(err) {
            if (err) {
                console.error(err);
                res.send("Status not updated");
            } else {
                res.send("Status updated");
            }
        });
    });

});

/* send the state of the questionnaire for an activity */
router.get('/getQuestionnaireState/:cpwd?', function(req, res, next) {
    var Activity = trigo.getActivity();
    var codepwdstring = req.params.cpwd;
    var code = codepwdstring.split(";")[0];
    var pwd = codepwdstring.split(";")[1];
    console.log(code + ';' + pwd);
    Activity.findOne({"code": code, "password": pwd}, function(err, activity){
        res.json(activity.questionnaire);
        console.log(JSON.stringify(activity.questionnaire));
    });
});


/* send the XML file fitting to the ID */
router.get('/getActivityXML/:aid?', function(req, res, next) {
  var Activity = trigo.getActivity();
  var aId = req.params.aid;
  Activity.findById(aId, function(err, activity){
    var filelist = activity.xmls;
    filelist.splice(0,1);
    activity.xmls = filelist;
    //activity.save();
    var file = appRoot+"/public/files/"+activity.id+"/tasklist"+filelist[0]+".xml";
    res.sendFile(file);
  });
});

function returnInt(element) {
  return parseInt(element, 10);
}

/* update the XML list of downloaded XMLs */
router.post('/updateXML/:cpwd?', function(req, res, next) {
  var Activity = trigo.getActivity();
  var codepwdstring = req.params.cpwd;
  var fulllist = [1,2,3,4,5,6,7,8,9,10,11,12,13,14];
  var diff = [];
  var times = ["","","","","","","","","","","","","",""];
  var code = codepwdstring.split(";")[0],
    pwd = codepwdstring.split(";")[1];

  Activity.findOne({"code": code, "password": pwd}, function(err, activity){
    if (err){
      console.error(err);
      res.end(err);
    } else {
      var updatelist = [];
      if (req.body.updatelist){
        updatelist = req.body.updatelist.map(Number).sort(function(a, b){return a-b});
      }
      var xmllist = [];
      //create a list with all indexes saved in the database
      activity.downloaded_xmls.forEach(function(elem, index) {
        xmllist.push(parseInt(elem.xml_index));
      });
      updatelist.forEach(function(ele, i){
        if (xmllist.indexOf(ele) != -1){ //check if index is already in database, if so, remove it, if not add it
          Activity.findByIdAndUpdate(
            activity._id,
            {$pull: {"downloaded_xmls": {xml_index: ele}}},
            {safe: true, upsert: true},
            function(err, model) {
              if (err){
                res.end(err);
                console.error(err);
              } else {

              }
            }
          );
        } else { //add index to database
          Activity.findByIdAndUpdate(
            activity._id,
            {$push: {"downloaded_xmls": {xml_index: ele, date: new Date()}}},
            {safe: true, upsert: true},
            function(err, model) {
              if (err){
                res.end(err);
                console.error(err);
              } else {

              }
            }
          );
        }
      });
      res.send("XML list updated");
    }
  });
});

/* send the XML file to an activity with the fitting code and pw. */
router.get('/getXML/:cpwd?', function(req, res, next) {
  var Activity = trigo.getActivity();
  var codepwdstring = req.params.cpwd;
  var code = codepwdstring.split(";")[0],
    pwd = codepwdstring.split(";")[1];
	
  Activity.findOne({"code": code, "password": pwd}, function(err, activity){
    if (err){
      console.error(err);
      res.end(err);
    } else if (!activity){ //check if the teacher pwd was given
      Activity.findOne({"code": code, "teacherpwd": pwd}, function(err, act){
        if (err){
          console.error(err);
        } else if (!act){ //no activity found
          console.log("no activity was found - probably wrong password or code");
          res.send("-1");
        } else {
          var f = appRoot+"/public/files/"+act.id+"/tasklist.teacher.xml";
          res.sendFile(f);
        }
      })

    } else {
      var count = 0,
        list = [],
        checklist = [1,2,3,4,5,6,7,8,9,10,11,12,13,14],
        difflist = [],
        index = 1;
      //check if downloaded_xmls exists and if so set the index to the item with the lowest number
      if (activity.downloaded_xmls){
        activity.downloaded_xmls.forEach(function(elem, index) {
          list.push(elem.xml_index);
          count++;
        });
        list = list.map(Number).sort(function(a, b){return a-b});
        //crate a list to assign the index to the lowest missing file
        checklist.forEach(function(key) {
          if (-1 === list.indexOf(key)) {
            difflist.push(key);
          }
        }, this);
        index = difflist[0];
      }
      if (count == 14){ //if already 14 xmls are downloaded send -2
        console.log("all XML files are downloaded");
        res.send("-2");
      } else{ //otherwise update the database and send the file
        Activity.findByIdAndUpdate(
          activity._id,
          {$push: {"downloaded_xmls": {xml_index: index, date: new Date()}}},
          {safe: true, upsert: true},
          function(err, model) {
            if (err){
              res.send(err);
              console.error(err);
            } else {
              var file = appRoot+"/public/files/"+activity.id+"/tasklist"+index+".xml";
              res.sendFile(file, function(err){
                 if(err){
                  console.error(err);
                  res.send(err);
                }
                 console.log("file tasklist"+index+".xml downloaded");
               //convert date to our format
                 var d =new Date();
                 var min = 0;
                 if (d.getMinutes() < 10)
                     min = "0" + d.getMinutes(); 
                 else
                     min = d.getMinutes();
                 var date = 0;
                 if (d.getDate() < 10)
                     date = "0" + d.getDate(); 
                 else
                     date = d.getDate(); 
                 var month = 0;
                 if (parseInt(d.getMonth()+1) < 10)
                    month = "0" + parseInt(d.getMonth()+1); 
                 else
                    month = parseInt(d.getMonth()+1);
                 var dateString = date + '/' + month + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + min
                 var socket = trigo.getSocket();//  console.log(io); 
                 console.log("connected, and emitting a download");
               	 //socket.emit('download', { activityID: activity._id,file: 'tasklist'+index+'.xml', date : dateString, group : index});
                 trigo.getIo().sockets.emit('download', { activityID: activity._id,file: 'tasklist'+index+'.xml', date : dateString, group : index});
                 
              });
            }
          }
        );
      }
    }
  });
});

function toStr(item) {
  return item.toString();
}

/* save the XML file fitting to the ID */
router.post('/saveXML/:cpwd?', function(req, res, next) {
  var Activity = trigo.getActivity();
  var codepwdstring = req.params.cpwd;
  var code = codepwdstring.split(";")[0],
    pwd = codepwdstring.split(";")[1];
  var f = null;
  if (req.files){
    f = req.files;
  } else
    console.log("There are no files in the request");

  Activity.findOne({"code": code, "password": pwd}, function(err, activity){
    if (err){
      console.error(err);
      res.end(err);
    } else if (!activity){
      console.log("no activity found, probably wrong code or password");
      res.send("-1 no activity found, probably wrong code or password");
    } else {
      if (f.fileToUpload.originalname != null){
        var groupName = f.fileToUpload.originalname.split("_")[0];
        var path = appRoot+"/public/files/"+activity.id+"/uploads/" + f.fileToUpload.originalname;

        // moving the file from uploads to path
        fse.move(f.fileToUpload.path, path, {clobber: true},  function(err) {
          if(err)
            return console.log("something went wrong while moving the file" );
          console.log("success moving the file!");
        });
        var todayDate = new Date();
        var index = groupName.split("Grupp")[1]
        Activity.findByIdAndUpdate(
          activity._id,
          {$push: {"uploaded_xmls": {xml_index: index, date: todayDate , filename: path}}},
          {safe: true, upsert: true},
          function(err, model) {
            if (err){
              res.send(err);
              console.error(err);
            } else {
              console.log("Upload saved in database")
              //convert date to our format
              var d = todayDate;
              var min = 0;
              if (d.getMinutes() < 10)
                  min = "0" + d.getMinutes(); 
              else
                  min = d.getMinutes();
              var date = 0;
              if (d.getDate() < 10)
                  date = "0" + d.getDate(); 
              else
                  date = d.getDate(); 
              var month = 0;
              if (parseInt(d.getMonth()+1) < 10)
                 month = "0" + parseInt(d.getMonth()+1); 
              else
                 month = parseInt(d.getMonth()+1);
              var dateString = date + '/' + month + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + min
              var socket = trigo.getSocket();//  console.log(io); 
              //socket.to(activity._id).emit('upload', {activityID: activity._id ,file: f.fileToUpload.originalname, date: dateString, group: index});
              //socket.emit('upload', {activityID: activity._id ,file: f.fileToUpload.originalname, date: dateString, group: index});
              
              //count number of files belonging to the same group
              var countFiles = 0;
              for (var j = 0; j <= activity.uploaded_xmls.length; j++) {
                  if (activity.uploaded_xmls[j]) {
                     if (index == activity.uploaded_xmls[j].xml_index){
                        countFiles++;
                     } 
                  }
              }
              trigo.getIo().sockets.emit('upload', {activityID: activity._id ,file: f.fileToUpload.originalname, date: dateString, group: index, countFiles: countFiles });
            }
          }
        );
        res.end("0 file uploaded correctly");// return 0 if there was not any error when uploading
      } else {
        console.log("no file in the request or name can't be retrieved");
        res.end("-2 no file in the request");// return -2 if there was not possible to upload due to not file in the request
      }
    }
  });
});


/* save the XML file fitting to the ID */
router.post('/saveXMLQuestionnaire/:cpwd?', function(req, res, next) {
    var Activity = trigo.getActivity();
    var codepwdstring = req.params.cpwd;
    var code = codepwdstring.split(";")[0],
        pwd = codepwdstring.split(";")[1];
    var f = null;
    if (req.files){
        f = req.files;
    } else
        console.log("There are no files in the request");

    Activity.findOne({"code": code, "password": pwd}, function(err, activity){
        if (err){
            console.error(err);
            res.end(err);
        } else if (!activity){
            console.log("no activity found, probably wrong code or password");
            res.send("-1 no activity found, probably wrong code or password");
        } else {
            if (f.fileToUpload.originalname != null){
                var groupName = f.fileToUpload.originalname.split("_")[0];
                var path = appRoot+"/public/files/"+activity.id+"/questionnaires/" + f.fileToUpload.originalname;

                // moving the file from uploads to path
                fse.move(f.fileToUpload.path, path, {clobber: true},  function(err) {
                    if(err)
                        return console.log("something went wrong while moving the file" );
                    console.log("success moving the file!");
                });
                var todayDate = new Date();
                var index = groupName.split("Grupp")[1]
                Activity.findByIdAndUpdate(
                    activity._id,
                    {$push: {"uploaded_xmls": {xml_index: index, date: todayDate , filename: path}}},
                    {safe: true, upsert: true},
                    function(err, model) {
                        if (err){
                            res.send(err);
                            console.error(err);
                        } else {
                            console.log("Upload saved in database")
                            //convert date to our format
                            var d = todayDate;
                            var min = 0;
                            if (d.getMinutes() < 10)
                                min = "0" + d.getMinutes();
                            else
                                min = d.getMinutes();
                            var date = 0;
                            if (d.getDate() < 10)
                                date = "0" + d.getDate();
                            else
                                date = d.getDate();
                            var month = 0;
                            if (parseInt(d.getMonth()+1) < 10)
                                month = "0" + parseInt(d.getMonth()+1);
                            else
                                month = parseInt(d.getMonth()+1);
                            var dateString = date + '/' + month + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + min
                            var socket = trigo.getSocket();//  console.log(io);
                            //socket.to(activity._id).emit('upload', {activityID: activity._id ,file: f.fileToUpload.originalname, date: dateString, group: index});
                            //socket.emit('upload', {activityID: activity._id ,file: f.fileToUpload.originalname, date: dateString, group: index});

                            //count number of files belonging to the same group
                            var countFiles = 0;
                            for (var j = 0; j <= activity.uploaded_xmls.length; j++) {
                                if (activity.uploaded_xmls[j]) {
                                    if (index == activity.uploaded_xmls[j].xml_index){
                                        countFiles++;
                                    }
                                }
                            }
                            trigo.getIo().sockets.emit('upload', {activityID: activity._id ,file: f.fileToUpload.originalname, date: dateString, group: index, countFiles: countFiles });
                        }
                    }
                );
                res.end("0 file uploaded correctly");// return 0 if there was not any error when uploading
            } else {
                console.log("no file in the request or name can't be retrieved");
                res.end("-2 no file in the request");// return -2 if there was not possible to upload due to not file in the request
            }
        }
    });
});

router.get('/getKML/:aid?/:sel?', function(req, res, next) {
  console.log("getKML was called with aid: " + req.params.aid);
  var Activity = trigo.getActivity();
  var aId = req.params.aid;
  var select = req.params.sel;

  Activity.findById(aId, function(err, activity){
    var phpcmd = "php -f "+appRoot+"/public/php/GenerateKML.php activity="+aId+" select="+select;

    exec(phpcmd, function (error, stdout, stderr) {
      if (error){
        console.error(error);
        console.error(stdout);
        res.send(stderr);
        console.log("error generating kml " + error);
      }else{
        var file = appRoot+"/public/files/"+aId+"/"+aId+".kml";
        res.download(file, activity.title+'.kml', function(err){
          if (err) {
            console.error(err);
            // Handle error, but keep in mind the response may be partially-sent
            // so check res.headersSent
          } else {
            console.log("success generating " + file);
          }
        });
      }
    });
  });
});

router.get('/createKML/:aid?/:sel?', function(req, res, next) {
    var Activity = trigo.getActivity();
    var aId = req.params.aid;
    var select = req.params.sel;

    Activity.findById(aId, function(err, activity){
        var phpcmd = "php -f "+appRoot+"/public/php/GenerateKML.php activity="+aId+" select="+select;
        //console.log(phpcmd);

        exec(phpcmd, function (error, stdout, stderr) {
            if (error){
                console.error(error);
                console.error(stdout);
                res.send(stderr);
            }else{
                var file = appRoot+"/public/files/"+aId+"/"+aId+".kml";
                res.sendFile(file, function(err){
                    if (err) {
                        console.error(err);
                        // Handle error, but keep in mind the response may be partially-sent
                        // so check res.headersSent
                    } else {
                        console.log("success generating " + file);
                    }
                });
            }
        });
    });
});


module.exports = router;
