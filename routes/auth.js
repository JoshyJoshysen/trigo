var bcrypt 			      = require('bcryptjs'),
    passport 		      = require('passport'),
    LocalStrategy     = require('passport-local').Strategy,
    FacebookStrategy  = require('passport-facebook').Strategy,
    TwitterStrategy   = require('passport-twitter').Strategy,
    GoogleStrategy    = require('passport-google-oauth').OAuth2Strategy;

var trigo   = require('../app'),
    oauth   = require('../oauth.js');

exports.getPassport = function() {
  return passport;
}

exports.initializeLogin = function() {
  // console.log("Passport: " + passport);

  // serialize and deserialize
  passport.serializeUser(function(user, done) {
    // console.log('serializeUser: ' + user._id)
    done(null, user._id);
  });
  passport.deserializeUser(function(id, done) {
    trigo.getUser().findById(id, function(err, user){
      //console.log(user)
      if(!err) done(null, user);
      else done(err, null)
    })
  });


  passport.use(new LocalStrategy(function(username, password, done) {
    console.log("new local strategy");
    process.nextTick(function() {
      console.log("next tick");
      trigo.getUser().findOne({
        'username': username
      }, function(err, user) {
        console.log("find user");
        if (err) {
          console.log("Error: " + err);
          return done(err);
        }

        if (!user) {
          console.log("no user");
          return done(null, false, {message: "The user does not exist."});
        }

        if (!bcrypt.compareSync(password, user.password)) {
          console.log("wrong password");
          return done(null, false, {message: "Wrong password."});
        }

        console.log("user: " + user);
        return done(null, user);
      });
    });
  }));

  passport.use(new FacebookStrategy({
      clientID: 	  oauth.facebook.appId,
      clientSecret: oauth.facebook.appSecret,
      callbackURL:  oauth.facebook.callback
    }, function(accessToken, refreshToken, profile, done) {
      process.nextTick(function() {
        //Assuming user exists
        // console.log([profile]);
        // profile.username = profile.displayName;
        // profile._id = new ObjectID(profile.id);
        // done(null, profile);
        var User = trigo.getUser();
        User.findOne({ oauthID: profile.id }, function(err, user) {
          if(err) { console.log(err); }

          if (!err && user != null) {
            done(null, user);
          } else {
            var user = new User({
              oauthID: profile.id,
              username: profile.displayName
            });
            user.save(function(err) {
              if(err) {
                console.log(err);
              } else {
                console.log("saving facebook user ...");
                done(null, user);
              };
            });
          };
        });

      });
    }
  ));

  passport.use(new TwitterStrategy({
      consumerKey: 	oauth.twitter.consumerKey,
      consumerSecret: oauth.twitter.consumerSecret,
      callbackURL: 	oauth.twitter.callback
    },
    function(token, tokenSecret, profile, done) {
      // User.findOrCreate(..., function(err, user) {
      //   if (err) { return done(err); }
      //   done(null, user);
      // });
      process.nextTick(function() {
        //Assuming user exists
        //done(null, profile);

        var User = trigo.getUser();
        User.findOne({ oauthID: profile.id }, function(err, user) {
          if(err) { console.log(err); }

          if (!err && user != null) {
            done(null, user);
          } else {
            user = new User({
              oauthID: profile.id,
              username: profile.displayName
            });
            user.save(function(err) {
              if(err) {
                console.log(err);
              } else {
                console.log("saving twitter user ...");
                done(null, user);
              };
            });
          };
        });
      });
    }
  ));

  passport.use(new GoogleStrategy({
      clientID: oauth.google.clientId,
      clientSecret: oauth.google.clientSecret,
      callbackURL: oauth.google.callback
    },
    function(accessToken, refreshToken, profile, done) {
      // User.findOrCreate({ openId: identifier }, function(err, user) {
      //  		done(err, user);
      // });
      process.nextTick(function() {
        //Assuming user exists
        //done(null, profile);

        var User = trigo.getUser();
        User.findOne({ oauthID: profile.id }, function(err, user) {
          if(err) { console.log(err); }

          if (!err && user != null) {
            done(null, user);
          } else {
            user = new User({
              oauthID: profile.id,
              username: profile.displayName
            });
            user.save(function(err) {
              if(err) {
                console.log(err);
              } else {
                console.log("saving google user ...");
                done(null, user);
              }
            });
          }
        });
      });
    }
  ));
}


exports.loggedIn = function(req, res, next) {
  if (req.user) {
    next();
  } else {
    res.redirect('/login');
  }
}

/*
 * GET login.
 */
exports.index = function(req, res) {
  if (req.user) {
    res.render('login',
      {
        logout: true,
        user: req.user
      });
  } else {
    //console.log(req.session.messages);
    res.render('login', { message: req.flash('error')[0] }); //, { message: req.session.messages }
    //req.session.messages = null;
  }
};

exports.logout = function(req, res) {
  req.logout();
  res.redirect('/login');
};


/************* LOCAL **************/
exports.local = passport.authenticate('local', {
  successReturnToOrRedirect: '/authoring',
  failureRedirect: '/login',
  failureFlash: true
});

/*********** FACEBOOK ************/
exports.facebook = passport.authenticate('facebook');

exports.facebookCallback =  passport.authenticate('facebook', {
  successReturnToOrRedirect: '/authoring',
  failureRedirect: '/login'
});

/*********** TWITTER ************/
exports.twitter = passport.authenticate('twitter');

exports.twitterCallback =  passport.authenticate('twitter', {
  successReturnToOrRedirect: '/authoring',
  failureRedirect: '/login'
});

/************ GOOGLE *************/
exports.google = passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.email'] });

exports.googleCallback =  passport.authenticate('google', {
  successReturnToOrRedirect: '/authoring',
  failureRedirect: '/login'
});



/*
 * Registration
 */

exports.reg = function(req, res) {
  res.render('registration');
};

exports.register = function(req, res) {
  console.log("Register new User");
  console.log("UN: " + req.body.username);
  console.log("PW: " + req.body.password);
  console.log("PW2: " + req.body.password2);

  var username  = req.body.username,
    password  = req.body.password,
    password2 = req.body.password2;

  var User = trigo.getUser();

  User.findOne({ username: username }, function (err, user) {
    if (err) return console.error(err);
    console.log(user);

    if (user == null) {
      console.log("Username is available.");

      if (password === password2) {
        console.log("valid password");

        var hash = bcrypt.hashSync(password, 10);

        var c = new User({username: username, password: hash});

        c.save(function(err, savedUser) {
          if (err){
            console.log(err);
            return;
          } else {
            console.log('User was created.');
            //res.redirect('/');

            res.redirect('/login');
          }
        });
      } else {
        console.log("The passwords did not match. Please try it again.");
        res.render('registration',
          {
            message: 'The passwords did not match. Please try again.',
            username: username
          });
      }

    } else {
      console.log("Username is already in use. Please choose another one.");
      res.render('registration', {message: 'Username is already in use. Please choose another one.'});
    }


  });
}