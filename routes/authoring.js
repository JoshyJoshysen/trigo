var express = require('express');
var fse = require('fs-extra');
var router = express.Router();
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
var xmlbuilder = require('xmlbuilder');
var trigo = require('../app');

/* GET home page. */
router.get('/', ensureLoggedIn('/login'), function(req, res, next) {
  var Activity = trigo.getActivity();
  Activity.find({ user: req.user._id })
    .sort({'name': 1})
    .exec(function(err, activities) {
      //find all ownActivities not created by the current user
      Activity.find({ user: { $ne: req.user._id } })
        .sort({'name': 1})
        .populate('user', 'username') //get the username of the user who created the scenario
        .exec(function(err, otherActivities){ //execute query
          res.render('activities', {
            user: req.user,
            activities: activities,
            otherActivities: otherActivities
          });
        });
    });
});

router.get('/visualizing_list', ensureLoggedIn('/login'), function(req, res, next) {
  res.render('visualization_list', {
    user: req.user
  });
});

/* Authoring environment */
router.get('/activity/:aid?', ensureLoggedIn('/login'), function(req, res, next){
  var aId       = req.params.aid, //get the id of the selected activity
      Activity  = trigo.getActivity();

  if (aId == "new"){
    Activity.findById(aId, function(err, activity){
      res.render('scenario', {
        user: req.user,
        scenario: activity
      });
    });
  } else {
    var dllist = [],
        templist = [],
        checklist = [1,2,3,4,5,6,7,8,9,10,11,12,13,14];



    Activity.findById(aId, function(err, activity){
      activity.downloaded_xmls.forEach(function(elem, index) {
        templist.push(parseInt(elem.xml_index));
      });
      checklist.forEach(function(key) {
        if (-1 === templist.indexOf(key)) {
          dllist.push(key);
        }
      }, this);
      res.render('dashboard', {
        user: req.user,
        activity: activity,
        points: activity.data,
        downloadlist: dllist,
        downloadtimes: activity.xmlstime
      });
    });
  }
});

/*
 * POST activity
 */
router.post('/save', ensureLoggedIn('/login'), function(req,res,next){
  var Activity = trigo.getActivity();

  function saveFailed() {
    res.end('Scenario saved failed');
  }

  var aId = req.body.id;

  //find the activity by ID
  Activity.findById(aId, function(err, scenario){
    //if the scenario does not exist yet or was created by another user,
    //create a new scenario
    if (!scenario || scenario.user + "" !== req.user._id + "") {
      req.body.user = req.user._id; //store id of user who created the scenario
      req.body.code = makeRandomText();
      req.body.password = makeRandomText();
      req.body.teacherpwd = makeRandomText();
      var filerray = [];
      for (var i=1; i <= 14; i++){
        filerray.push(i);
      }
      req.body.xmls = filerray;

      var a = new Activity(req.body); //create a new scenario

      a.save(function(err, savedScenario) {
        if (err){
          console.error(err);
          return saveFailed();
        } else {
          createXML(savedScenario.data, savedScenario.id, req.body.code, req.body.password);
          createTeacherXML(savedScenario.data, savedScenario.id, req.body.code, req.body.teacherpwd);
          res.send(savedScenario);
        }
      });
    }
    //if the scenario exists already, update it
    else {
      console.log("something went wrong");
      res.end("something went wrong!");
    }
  });
});

router.get('/reset/:aid?', ensureLoggedIn('/login'), function(req,res,next){
  var aId       = req.params.aid, //get the id of the selected activity
      Activity  = trigo.getActivity();

  Activity.findById(aId, function(err, activity){
    if (err){
      console.error(err);
    } else {
      activity.xmls = [1,2,3,4,5,6,7,8,9,10,11,12,13,14];
      activity.submitted = [];
      activity.xmlstime = [];
      activity.downloaded_xmls = [];
      activity.uploaded_xmls = [];
      activity.save(function (err, removedact) {
        if (err){
          console.error(err);
        } else {
          console.log("activity reseted");
          //var path = appRoot+"/public/files/"+activity.id+"/uploads/";
          //deleteFolder(path);
          res.redirect("/authoring/activity/"+activity.id);
        }
      });
    }
  });
});

/* Delete an activity by ID */
router.get('/delete/:aid?/:user?', ensureLoggedIn('/login'), function(req, res, next){
  var aId       = req.params.aid, //get the id of the selected activity
      Activity  = trigo.getActivity();

  Activity.findById(aId, function(err, activity){
    if (err){
      console.error(err);
    } else {
      activity.remove(function (err, removedact) {
        if (err){
          console.error(err);
        } else {
          console.log('USER A: ' + activity.user);
          console.log('USER B: ' + req.params.user);
          if (activity.user == req.params.user) {
            var path = appRoot+"/public/files/"+removedact.id+"/";
            deleteFolder(path);
            console.log("activity deleted");
            res.send("activity deleted");
          } else {
            res.send("This user is not allowed to delete this activity");
          }
        }
      });

    }
  });
});


function createXML(points, actID, code, pwd){
  for (var j = 1; j <= 14; j++){ //start loop to create 14 XML files
    var xml = createXMLHead(points, j, code, pwd);

    /*//begin with the "head" of the XML file (always the same with the info of the points created before)
    var xml = xmlbuilder.create('activity',{version: '1.0', encoding: 'UTF-8'});
    var groupname = xml.ele('groupname','Grupp'+j);
    var imgfolder = xml.ele('imagefolder', 'GRM/images');
    var refs = xml.ele('references');
    //create 6 points from the references in
    for (var i = 0; i < 6; i++){ // begin loop to add 6 points to the "head"
      var point = refs.ele('point',{'id':i+1});
      var img = point.ele('img',imgs[i]); //get the references from the array
      var lat = point.ele('lat',points["point"+i].lat);
      var lon = point.ele('lon',points["point"+i].lng);
    } //end of the loop for the adding the 6 points
    //end of the "head" of the XML file*/

    //begin with the body of the xml file
    var task = xml.ele('task', {id: j});
    var taskpoints = getTaskPoints()
    for (var int = 1; int <= 10; int++){ //loop to create 10 tasks in a random order
      var rnd = Math.floor((Math.random() * taskpoints.length));
      var tpoint = taskpoints[rnd];
      taskpoints.splice(rnd, 1);
      switch (int){ //switch over the possibilities of the randomize to add a random task
        case 1:
          var point = task.ele('point', {id:int});
          var dist1 = point.ele('distance', {ref:tpoint.ref1}, tpoint.val1);
          var dist2 = point.ele('distance', {ref:tpoint.ref2}, tpoint.val2);
          break;
        case 2:
          var point = task.ele('point', {id:int});
          var dist1 = point.ele('distance', {ref:tpoint.ref1}, tpoint.val1);
          var dist2 = point.ele('distance', {ref:tpoint.ref2}, tpoint.val2);
          break;
        case 3:
          var point = task.ele('point', {id:int});
          var dist1 = point.ele('distance', {ref:tpoint.ref1}, tpoint.val1);
          var dist2 = point.ele('distance', {ref:tpoint.ref2}, tpoint.val2);
          break;
        case 4:
          var point = task.ele('point', {id:int});
          var dist1 = point.ele('distance', {ref:tpoint.ref1}, tpoint.val1);
          var dist2 = point.ele('distance', {ref:tpoint.ref2}, tpoint.val2);
          break;
        case 5:
          var point = task.ele('point', {id:int});
          var dist1 = point.ele('distance', {ref:tpoint.ref1}, tpoint.val1);
          var dist2 = point.ele('distance', {ref:tpoint.ref2}, tpoint.val2);
          break;
        case 6:
          var point = task.ele('point', {id:int});
          var dist1 = point.ele('distance', {ref:tpoint.ref1}, tpoint.val1);
          var dist2 = point.ele('distance', {ref:tpoint.ref2}, tpoint.val2);
          break;
        case 7:
          var point = task.ele('point', {id:int});
          var dist1 = point.ele('distance', {ref:tpoint.ref1}, tpoint.val1);
          var dist2 = point.ele('distance', {ref:tpoint.ref2}, tpoint.val2);
          break;
        case 8:
          var point = task.ele('point', {id:int});
          var dist1 = point.ele('distance', {ref:tpoint.ref1}, tpoint.val1);
          var dist2 = point.ele('distance', {ref:tpoint.ref2}, tpoint.val2);
          break;
        case 9:
          var point = task.ele('point', {id:int});
          var dist1 = point.ele('distance', {ref:tpoint.ref1}, tpoint.val1);
          var dist2 = point.ele('distance', {ref:tpoint.ref2}, tpoint.val2);
          break;
        case 10:
          var point = task.ele('point', {id:int});
          var dist1 = point.ele('distance', {ref:tpoint.ref1}, tpoint.val1);
          var dist2 = point.ele('distance', {ref:tpoint.ref2}, tpoint.val2);
          break;
      }//end of switch case
    }//end of loop for random 10 tasks

    var file = appRoot+"/public/files/"+actID+"/tasklist"+j+".xml";
    writeXMLFile(file, xml);
    //write a XML file


  }//end of loop for 14 XML files
}
/**
 * Creates the XML file for the teacher
 * @param points
 * @param actID
 */

function createTeacherXML(points, actID, code, pwd){
  //creates the head of the XML file where 0 is for teachers
  var xml = createXMLHead(points, 0, code, pwd);

  //start with the body of the TeacherXML
  var task = xml.ele('task', {id: 1});
  for (var i = 1; i <= 6; i++){
    var point = task.ele('point', {id:i});
    var dist1 = point.ele('distance', {ref:i}, 0);
    var dist2 = point.ele('distance', {ref:i}, 0);
  }

  var file = appRoot+"/public/files/"+actID+"/tasklist.teacher.xml";
  writeXMLFile(file, xml);
}

/**
 * Writes the XML
 * @param path - location where to write
 * @param file - the XML file
 */
function writeXMLFile(path, file){
  fse.outputFile(path, file.toString({pretty: true}), function(err) {
    if (err){
      console.error(err)
    }
  });
}

/**
 * Delete the folder of an activity
 * @param actID - the ID of the activity
 */
function deleteFolder(path){
  fse.remove(path, function(err) {
    if (err){
      console.error(err);
    } else {
      console.log("folder "+path+" deleted.");
    }
  });
}

/**
 * Creates the Head of the XML file
 * @param points - contains the 6 points of the tennis court
 * @param pos - position in the loop for the group
 * @returns {xml body with the 6 points of the tennis court}
 */
function createXMLHead(points, pos, code, pwd){
  var imgs = ["cat.jpg", "house.jpg", "tree.jpg", "bike.jpg", "car.jpg", "horse.jpg"]; //image list for the reference points

  //begin with the "head" of the XML file (always the same with the info of the points created before)
  var xml = xmlbuilder.create('activity',{version: '1.0', encoding: 'UTF-8'});
  if (pos == 0){
    var groupname = xml.ele('groupname','Lärare');
  } else {
    var groupname = xml.ele('groupname','Grupp'+pos);
  }
  var code = xml.ele('code', code);
  var pwd = xml.ele('password', pwd);
  var imgfolder = xml.ele('imagefolder', 'GEM/images');
  var refs = xml.ele('references');
  //create 6 points from the references in
  for (var i = 0; i < 6; i++){ // begin loop to add 6 points to the "head"
    var point = refs.ele('point',{'id':i+1});
    var img = point.ele('img',imgs[i]); //get the references from the array
    var lat = point.ele('lat',points["point"+i].lat);
    var lon = point.ele('lon',points["point"+i].lng);
  } //end of the loop for the adding the 6 points
  //end of the "head" of the XML file
  return xml;
}


/**
 * Creates a random string with 4 letters
 * @returns {string that contains 4 letters}
 */

function makeRandomText(){
  var text = "";
  var possible = "abcdefghijklmnopqrstuvwxyz";

  for( var i=0; i < 4; i++ )
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
/**
 * create a list with the reference points for the tasks to ranomize it later
 * @returns {Array} with info about the references points
 */
function getTaskPoints(){
  var taskpoints = [];

  //create a list with the 10 tasks to randomize them later
  for (var i = 1; i <=10; i++){
    switch (i) {
      case 1:
        var temp = new Object();
        temp["ref1"] = 1;
        temp["val1"] = 20;
        temp["ref2"] = 2;
        temp["val2"] = 30;
        taskpoints.push(temp);
        break;
      case 2:
        var temp = new Object();
        temp["ref1"] = 3;
        temp["val1"] = 20;
        temp["ref2"] = 4;
        temp["val2"] = 15;
        taskpoints.push(temp);
        break;
      case 3:
        var temp = new Object();
        temp["ref1"] = 1;
        temp["val1"] = 25;
        temp["ref2"] = 2;
        temp["val2"] = 25;
        taskpoints.push(temp);
        break;
      case 4:
        var temp = new Object();
        temp["ref1"] = 4;
        temp["val1"] = 65;
        temp["ref2"] = 6;
        temp["val2"] = 30;
        taskpoints.push(temp);
        break;
      case 5:
        var temp = new Object();
        temp["ref1"] = 1;
        temp["val1"] = 50;
        temp["ref2"] = 2;
        temp["val2"] = 25;
        taskpoints.push(temp);
        break;
      case 6:
        var temp = new Object();
        temp["ref1"] = 5;
        temp["val1"] = 10;
        temp["ref2"] = 6;
        temp["val2"] = 55;
        taskpoints.push(temp);
        break;
      case 7:
        var temp = new Object();
        temp["ref1"] = 5;
        temp["val1"] = 30;
        temp["ref2"] = 6;
        temp["val2"] = 60;
        taskpoints.push(temp);
        break;
      case 8:
        var temp = new Object();
        temp["ref1"] = 2;
        temp["val1"] = 15;
        temp["ref2"] = 3;
        temp["val2"] = 10;
        taskpoints.push(temp);
        break;
      case 9:
        var temp = new Object();
        temp["ref1"] = 1;
        temp["val1"] = 35;
        temp["ref2"] = 3;
        temp["val2"] = 50;
        taskpoints.push(temp);
        break;
      case 10:
        var temp = new Object();
        temp["ref1"] = 6;
        temp["val1"] = 30;
        temp["ref2"] = 1;
        temp["val2"] = 20;
        taskpoints.push(temp);
        break;
    }
  }
  return taskpoints;
}

module.exports = router;
