var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/background', function(req, res, next) {
  res.render('background', {
    title: 'TriGO - Background',
    user: req.user
  });
});

router.get('/tutorials', function(req, res, next) {
  res.render('tutorials', {
    title: 'TriGO - Tutorials',
    user: req.user
  });
});

router.get('/contact', function(req, res, next) {
  res.render('contact', {
    title: 'TriGO - Contact Us',
    user: req.user
  });
});

router.get('/downloads', function(req, res, next) {
  res.render('downloads', {
    title: 'TriGO - Downloads',
    user: req.user
  });
});


module.exports = router;
