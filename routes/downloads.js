var express = require('express');
var router = express.Router();

/* download the lite android HTML5 version of the mobile application */
router.get('/lite', function(req, res, next) {
  var file = appRoot+"/public/clients/TriGO_lite.apk";
  res.download(file, "TriGO_lite.apk", function(err){
    if (err) {
      console.error(err);
      // Handle error, but keep in mind the response may be partially-sent
      // so check res.headersSent
    } else {
      console.log("HTML5 apk downloaded");
    }
  });
});

/* download the native android HTML5 version of the mobile application */
router.get('/advanced', function(req, res, next) {
  var file = appRoot+"/public/clients/TriGO_advanced.apk";
  res.download(file, "TriGO_expert.apk", function(err){
    if (err) {
      console.error(err);
      // Handle error, but keep in mind the response may be partially-sent
      // so check res.headersSent
    } else {
      console.log("Advanced apk downloaded");
    }
  });
});

module.exports = router;
