var express = require('express');
var router = express.Router();
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('test', { title: 'TriGO Authoring' });
});

module.exports = router;
