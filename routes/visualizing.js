var express = require('express');
var fse = require('fs-extra');
var router = express.Router();
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
var xmlbuilder = require('xmlbuilder');
var trigo = require('../app');

/* GET visualization page. */
router.get('/:aid?/:sel?/:tit?', ensureLoggedIn('/login'), function(req, res, next) {
    var aId = req.params.aid;
    var select = req.params.sel;
    var title = req.params.tit;

    res.render('visualizing', {
        user: req.user,
        activityID: aId,
        select: select,
        activityTitle: title
    });
});

module.exports = router;
